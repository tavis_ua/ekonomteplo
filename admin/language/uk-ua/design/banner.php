<?php
// Heading
$_['heading_title'] = 'Банери';

// Text
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_list'] = 'Банери';
$_['text_add'] = 'Додати';
$_['text_edit'] = 'Редагувати';
$_['text_default'] = 'За замовчуванням';
$_['text_image'] = 'Зображення';
$_['text_bacground'] = 'Фон';
$_['text_html'] = 'HTML код';



// Column
$_['column_name'] = 'Назва банера';
$_['column_status'] = 'Статус';
$_['column_addition_classes'] = 'Додаткові класи';
$_['column_action'] = 'Дія';

$_['save_action'] = 'Зберегти';
$_['cancel_action'] = 'Відмінити';

// Entry
$_['html_form_title'] = 'HTML код банера';
$_['entry_name'] = 'Назва банера';
$_['entry_title'] = 'Заголовок';
$_['entry_link'] = 'Посилання';
$_['entry_image'] = 'Зображення';
$_['entry_background'] = 'Фон';
$_['entry_content'] = 'Контент';
$_['entry_type_content'] = 'Тип контенту';
$_['entry_status'] = 'Статус';
$_['entry_addition_classes'] = 'Додаткові класи';
$_['entry_sort_order'] = 'Порядок сортування';

// Error
$_['error_permission'] = 'У Вас немає прав для зміни налаштувань!';
$_['error_name'] = 'Назва банера повинно бути від 3 до 64 символів!';
$_['error_title'] = 'Заголовок повинен бути від 2 до 64 символів!';

//Help
$_['help_addition_classes'] = 'Назва класів, що буде додано до блоку банера';

