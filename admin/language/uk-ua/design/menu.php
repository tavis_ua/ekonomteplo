<?php
// Heading
$_['heading_title'] = 'Налаштування меню';

// Текст
$_['text_success'] = 'Операція виконана успішно!';
$_['text_list'] = 'Список меню';
$_['text_add'] = 'Додати меню';
$_['text_add_item'] = 'Додати пункт меню';
$_['text_edit'] = 'Редагувати пункт меню';
$_['text_remove_item'] = 'Видалити пункт меню';
$_['text_default'] = 'За замовчуванням';

// Стовпець
$_['column_menu_alias'] = 'Назва меню';
$_['column_menu_item'] = 'Пункт меню';
$_['column_language'] = 'Мова';
$_['column_action'] = 'Дія';

$_['entry_menu_item'] = 'Пункт меню';
$_['entry_menu_item_name'] = 'Назва пункту меню';
$_['entry_menu_item_link'] = 'Посилання';
$_['entry_menu_item_sort_order'] = 'Сорутвання';

// Помилка
$_['error_permission'] = 'Попередження: у вас немає дозволу на меню!';
$_['error_exists'] = 'Вже існує меню з таким ім\'ям';
$_['error_empty_name'] = 'Меню не містить назву';