<?php
// Heading
$_['heading_title'] = 'Статті';
$_['heading_category_title'] = 'Категорії';

// Text
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_list'] = 'Статті';
$_['text_category_list'] = 'Категорії';
$_['text_add'] = 'Додати';
$_['text_edit'] = 'Редагувати';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'Основний магазин';
$_['text_option'] = 'Параметр';
$_['text_option_value'] = 'Значення опції';
$_['text_percent'] = 'Відсоток';
$_['text_amount'] = 'Фіксована сума';

// Column
$_['column_name'] = 'Назва статті';
$_['column_status'] = 'Статус';
$_['column_date_start'] = 'Дата початку публікації';
$_['column_date_end'] = 'Дата закінчення публікації';
$_['column_action'] = 'Дія';

// Column category
$_['column_category_name'] = 'Назва категорії';
$_['column_category_status'] = 'Статус';
$_['column_category_action'] = 'Дія';

//Post type
$_['posts']         = 'Статті';
$_['services']      = 'Послуги';
$_['last_job']      = 'Останні роботи';
$_['executed_jobs'] = 'Виконані роботи';
$_['advantage_post'] = 'Переваги';

// Entry
$_['entry_name'] = 'Назва статті';
$_['entry_category_name'] = 'Назва категорії';
$_['entry_parent'] = 'Батьківська категорія';
$_['entry_top'] = 'Головне меню';
$_['entry_empty_link'] = 'Без посилання';
$_['entry_sub_name'] = 'Підзаголовок';
$_['entry_description'] = 'Опис';
$_['entry_post_text'] = 'Текст';
$_['entry_post_type'] = 'Тип статті';
$_['entry_meta_title'] = 'Мета-тег Title';
$_['entry_meta_keyword'] = 'Мета-тег Keyword';
$_['entry_meta_description'] = 'Мета-тег Description';
$_['entry_keyword'] = 'SEO URL';
$_['entry_model'] = 'Модель';
$_['entry_sku'] = 'Артикул';
$_['entry_upc'] = 'UPC';
$_['entry_ean'] = 'EAN';
$_['entry_jan'] = 'JAN';
$_['entry_isbn'] = 'ISBN';
$_['entry_mpn'] = 'MPN';
$_['entry_location'] = 'Розташування';
$_['entry_shipping'] = 'Необхідна доставка';
$_['entry_manufacturer'] = 'Виробник';
$_['entry_store'] = 'Магазини';
$_['entry_date_available'] = 'Дата надходження';
$_['entry_quantity'] = 'Кількість';
$_['entry_minimum'] = 'Мінімальна кількість';
$_['entry_stock_status'] = 'Відсутність на складі';
$_['entry_price'] = 'Ціна';
$_['entry_tax_class'] = 'Податок';
$_['entry_points'] = 'Бали';
$_['entry_option_points'] = 'Бали';
$_['entry_subtract'] = 'Віднімати зі складу';
$_['entry_weight_class'] = 'Одиниця виміру ваги';
$_['entry_weight'] = 'Вага';
$_['entry_dimension'] = 'Розміри (Д x Ш x В)';
$_['entry_length_class'] = 'Одиниця вимірювання довжини';
$_['entry_length'] = 'Довжина';
$_['entry_width'] = 'Ширина';
$_['entry_height'] = 'Висота';
$_['entry_image'] = 'Зображення статті';
$_['entry_additional_image'] = 'Додаткові зображення';
$_['entry_customer_group'] = 'Група клієнтів';
$_['entry_date_start'] = 'Дата початку';
$_['entry_date_end'] = 'Дата закінчення';
$_['entry_priority'] = 'Пріоритет';
$_['entry_attribute'] = 'Атрибут (характеристики)';
$_['entry_attribute_group'] = 'Групи атрибутів';
$_['entry_text'] = 'Значення';
$_['entry_option'] = 'Параметр';
$_['entry_option_value'] = 'Значення опції';
$_['entry_required'] = 'Обов&#39;язково';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Порядок сортування';
$_['entry_category'] = 'Показувати в категоріях';
$_['entry_services'] = 'Показувати в послугах';
$_['entry_filter'] = 'Фільтри';
$_['entry_download'] = 'Завантаження';
$_['entry_related'] = 'Схожі об\'єкти';
$_['post_entry_related'] = 'Рекомендовані товари';
$_['entry_tag'] = 'Теги статті';
$_['entry_reward'] = 'Бонусні бали';
$_['entry_layout'] = 'Макет';
$_['entry_recurring'] = 'Профіль підписки';
$_['entry_product_name'] = 'Назва';
$_['entry_product_name_placeholder'] = 'Назва обладнання';
$_['entry_product_count'] = 'Кількість';

// Help
$_['help_keyword'] = 'Повинно бути унікальним на всю систему і без пробілів.';
$_['help_sku'] = 'SKU або код виробника';
$_['help_upc'] = 'Універсальний код товару';
$_['help_ean'] = 'Європейський код товару';
$_['help_jan'] = 'Японський код товару';
$_['help_isbn'] = 'Міжнародний стандарт номера книги';
$_['help_mpn'] = 'Номер виробника';
$_['help_manufacturer'] = '(Автозаповнення)';
$_['help_minimum'] = 'Мінімальна кількість товару у замовленні (менше цієї к-ті товарів, додавання в корзину буде заборонено )';
$_['help_stock_status'] = 'Статус показується, коли к-ть товару 0, немає на складі';
$_['help_points'] = 'Кількість балів для купівлі товару. Поставте 0, щоб товар можна було придбати за бонусні бали.';
$_['help_category'] = '(Автозаповнення)';
$_['help_service'] = '(Автозаповнення)';
$_['help_filter'] = '(Автозаповнення)';
$_['help_download'] = '(Автозаповнення)';
$_['help_related'] = '(Автозаповнення)';
$_['help_tag'] = 'теги розділяються комою';
$_['help_empty_link'] = 'Пункт меню с пустим посиланням. Використовується тільки для відображення підменю';
$_['help_top'] = 'Показувати в головному меню.';

// Error
$_['error_warning'] = 'Уважно перевірте форму на помилки!';
$_['error_permission'] = 'У Вас немає прав для зміни товарів!';
$_['error_name'] = 'Назва статті повинна містити від 3 до 255 символів!';
$_['error_meta_title'] = 'Мета-тег Title повинен містити від 3 до 255 символів!';
$_['error_model'] = 'Модель товару повинна містити від 3 до 64 символів!';
$_['error_keyword'] = 'SEO URL зайнятий!';
$_['error_not_numeric_symbol'] = 'Поле має містити тільки числові значення';