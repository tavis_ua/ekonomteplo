<?php
// Heading
$_['heading_title'] = 'Статті';

// Text
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_list'] = 'Товари';
$_['text_add'] = 'Додати';
$_['text_extension'] = 'Розширення';
$_['text_edit'] = 'Редагувати';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'Основний магазин';
$_['text_option'] = 'Параметр';
$_['text_option_value'] = 'Значення опції';
$_['text_percent'] = 'Відсоток';
$_['text_extension '] = 'Розширення';
$_['text_amount'] = 'Фіксована сума';
$_['type_post'] = 'Тип статті';
$_['not_selected'] = 'Не вибрано';
$_['advantage'] = 'Переваги';
$_['services'] = 'Послуги';
$_['posts'] = 'Статті';
$_['completed'] = 'Реалізовані об\'єкти';
$_['similar'] = 'Схожі об\'єкти';

// Entry
$_['entry_name'] = 'Назва модуля';
$_['entry_status'] = 'Статус';

//Post type
$_['posts']         = 'Статті';
$_['services']      = 'Послуги';
$_['last_job']      = 'Останні роботи';
$_['executed_jobs'] = 'Виконані роботи';
$_['advantage_post'] = 'Переваги';

// Column
$_['column_name'] = 'Назва типу статті';
$_['column_model'] = 'Модель';
$_['column_image'] = 'Зображення';
$_['column_price'] = 'Ціна на сайті';
$_['column_quantity'] = 'Кількість';
$_['column_status'] = 'Статус';
$_['column_action'] = 'Дія';

// Entry
$_['entry_name'] = 'Назва типу статті';
$_['entry_description'] = 'Опис';
$_['entry_meta_title'] = 'Мета-тег Title';
$_['entry_meta_keyword'] = 'Мета-тег Keyword';
$_['entry_meta_description'] = 'Мета-тег Description';
$_['entry_keyword'] = 'SEO URL';
$_['entry_store'] = 'Магазини';
$_['entry_image'] = 'Зображення товару';
$_['entry_additional_image'] = 'Додаткові зображення';
$_['entry_customer_group'] = 'Група клієнтів';
$_['entry_date_start'] = 'Дата початку';
$_['entry_date_end'] = 'Дата закінчення';
$_['entry_priority'] = 'Пріоритет';
$_['entry_attribute'] = 'Атрибут (характеристики)';
$_['entry_attribute_group'] = 'Групи атрибутів';
$_['entry_text'] = 'Значення';
$_['entry_option'] = 'Параметр';
$_['entry_option_value'] = 'Значення опції';
$_['entry_required'] = 'Обов&#39;язково';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Порядок сортування';
$_['entry_category'] = 'Показувати в категоріях';
$_['entry_services'] = 'Показувати в послугах';
$_['entry_filter'] = 'Фільтри';
$_['entry_download'] = 'Завантаження';
$_['entry_related'] = 'Рекомендовані товари';
$_['entry_tag'] = 'Теги товару';
$_['entry_reward'] = 'Бонусні бали';
$_['entry_layout'] = 'Макет';
$_['entry_recurring'] = 'Профіль підписки';

// Help
$_['help_keyword'] = 'Повинно бути унікальним на всю систему і без пробілів.';
$_['help_sku'] = 'SKU або код виробника';
$_['help_upc'] = 'Універсальний код товару';
$_['help_ean'] = 'Європейський код товару';
$_['help_jan'] = 'Японський код товару';
$_['help_isbn'] = 'Міжнародний стандарт номера книги';
$_['help_mpn'] = 'Номер виробника';
$_['help_manufacturer'] = '(Автозаповнення)';
$_['help_minimum'] = 'Мінімальна кількість товару у замовленні (менше цієї к-ті товарів, додавання в корзину буде заборонено )';
$_['help_stock_status'] = 'Статус показується, коли к-ть товару 0, немає на складі';
$_['help_points'] = 'Кількість балів для купівлі товару. Поставте 0, щоб товар можна було придбати за бонусні бали.';
$_['help_category'] = '(Автозаповнення)';
$_['help_filter'] = '(Автозаповнення)';
$_['help_download'] = '(Автозаповнення)';
$_['help_related'] = '(Автозаповнення)';
$_['help_tag'] = 'теги розділяються комою';

// Error
$_['error_warning'] = 'Уважно перевірте форму на помилки!';
$_['error_permission'] = 'У Вас немає прав для зміни товарів!';
$_['error_name'] = 'Назва товару повинна містити від 3 до 255 символів!';
$_['error_meta_title'] = 'Мета-тег Title повинен містити від 3 до 255 символів!';
$_['error_model'] = 'Модель товару повинна містити від 3 до 64 символів!';
$_['error_keyword'] = 'SEO URL зайнятий!';