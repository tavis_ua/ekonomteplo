<?php
// Heading
$_['heading_title'] = 'Попередній перегляд статей';

// Text
$_['text_extension'] = 'Розширення';
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_edit'] = 'Налаштування модуля';
$_['type_slider'] = 'Слайдер';
$_['type_block'] = 'Блок';
$_['type_row'] = 'Строка';

// Entry
$_['entry_name'] = 'Назва модуля';
$_['entry_title'] = 'Заголовок';
$_['entry_description'] = 'Текст';
$_['entry_status'] = 'Статус';
$_['entry_width'] = 'Ширина';
$_['entry_height'] = 'Висота';
$_['entry_limit'] = 'Кількість одночасно відображених блоків';
$_['entry_post_type'] = 'Тип статті';
$_['entry_block_type_of_preview_title'] = 'Тип блока попереднього перегляду';

$_['not_selected'] = 'Не вибрано';
$_['services'] = 'Послуги';
$_['posts'] = 'Статті';
$_['completed'] = 'Реалізовані об\'єкти';
$_['similar'] = 'Схожі об\'єкти';
$_['last_job']      = 'Останні роботи';
$_['executed_jobs'] = 'Виконані роботи';
$_['job_review'] = 'Відгуки виконаних робіт';
$_['product_review'] = 'Відгуки товарів';

// Error
$_['error_permission'] = 'У Вас немає прав для управління даним модулем!';
$_['error_name'] = 'Назва модуля повинно містити від 3 до 64 символів!';