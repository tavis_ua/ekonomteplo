<?php

$_['column_name']   = 'Стікери';
$_['column_action'] = 'Дії';

$_['help_filename'] = 'Необхідно вибрати файл для завантаження';

$_['entry_filename']= 'Стікер';
$_['entry_description']= 'Опис';


$_['heading_title']  = 'Стікери';

$_['text_loading']  = 'Завантаження...';
$_['button_upload'] = 'Завантажити';
$_['filename']      = 'Немає стікеру';
$_['text_form']     = 'Додати стікер';
$_['text_list']     = 'Стікери';
$_['text_attribute']= 'Атрибут';

