<?php
// Heading
$_['heading_title'] = 'Категорії';

// Text
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_list'] = 'Категорії';
$_['text_add'] = 'Додати';
$_['text_edit'] = 'Редагувати';
$_['text_default'] = 'Основний магазин';
$_['text_image'] = 'Зображення';
$_['text_html'] = 'HTML код';

$_['save_action'] = 'Зберегти';
$_['cancel_action'] = 'Відмінити';

// Column
$_['column_name'] = 'Категорії';
$_['column_sort_order'] = 'Порядок сортування';
$_['column_action'] = 'Дія';

// Entry
$_['html_form_title'] = 'HTML код банера';
$_['entry_name'] = 'Категорія';
$_['entry_description'] = 'Опис';
$_['entry_meta_title'] = 'Мета-тег Title';
$_['entry_meta_keyword'] = 'Мета-тег Keywords';
$_['entry_meta_description'] = 'Мета-тег Description';
$_['entry_keyword'] = 'SEO URL';
$_['entry_parent'] = 'Батьківська категорія';
$_['entry_post'] = 'Стаття';
$_['entry_filter'] = 'Фільтри';
$_['entry_store'] = 'Магазини';
$_['entry_image'] = 'Зображення категорії';
$_['entry_top'] = 'Головне меню';
$_['entry_empty_link'] = 'Без посилання';
$_['entry_column'] = 'Стовбці';
$_['entry_sort_order'] = 'Порядок сортування';
$_['entry_status'] = 'Статус';
$_['entry_layout'] = 'Макет';
$_['entry_banner'] = 'Банери';
$_['entry_banner_name'] = 'Назва банера';
$_['entry_banner_title'] = 'Заголовок';
$_['entry_link'] = 'Посилання';
$_['entry_image'] = 'Зображення';
$_['entry_type_content'] = 'Тип контенту';
$_['entry_content'] = 'Контент';

// Help
$_['help_filter'] = '(Автозаповнення)';
$_['help_keyword'] = 'Повинно бути унікальним на всю систему.';
$_['help_top'] = 'Показувати в головному меню (тільки для головних батьківських категорій).';
$_['help_empty_link'] = 'Пункт меню с пустим посиланням. Використовується тільки для відображення підменю';
$_['help_column'] = 'Кількість стовпців у випадаючому меню категорії (тільки для головних батьківських категорій)';

// Error
$_['error_warning'] = 'Уважно перевірте форму на помилки!';
$_['error_permission'] = 'У Вас немає прав для зміни категорій!';
$_['error_name'] = 'Назва категорії повинне бути від 2 до 255 символів!';
$_['error_meta_title'] = 'Ключове слово повинно бути від 3 до 255 символів!';
$_['error_keyword'] = 'SEO URL зайнятий!';
$_['error_parent'] = 'Батьківська категорія обрана неправильно!';