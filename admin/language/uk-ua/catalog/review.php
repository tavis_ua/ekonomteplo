<?php
// Heading
$_['heading_title'] = 'Відгуки';

// Text
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_list'] = 'Відгуки';
$_['text_add'] = 'Додати';
$_['text_edit'] = 'Редагувати';

// Column
$_['column_product'] = 'Товар';
$_['column_author'] = 'Автор';
$_['column_rating'] = 'Рейтинг';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата';
$_['column_action'] = 'Дія';


$_['type_review'] = 'Тип відгука';

// Entry
$_['entry_product'] = 'Товар';
$_['column_type'] = 'Тип';
$_['entry_post']   = 'Стаття';
$_['entry_author'] = 'Автор';
$_['entry_rating'] = 'Рейтинг';
$_['entry_status'] = 'Статус';
$_['entry_text'] = 'Текст';
$_['entry_date_added'] = 'Дата';
$_['entry_prositive_text'] = 'Переваги';
$_['entry_negative_text'] = 'Недоліки';

// Help
$_['help_product'] = '(Автозаповнення)';

// Error
$_['error_permission'] = 'У Вас немає прав для зміни відгуків!';
$_['error_product'] = 'Потрібно вибрати товар!';
$_['error_author'] = 'Ім&#39;я автора повинно містити від 3 до 64 символів!';
$_['error_text'] = 'Текст відгуку повинен містити хоча б 1 символ!';
$_['error_rating'] = 'Потрібно встановити рейтинг!';