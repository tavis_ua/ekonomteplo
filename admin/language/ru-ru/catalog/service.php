<?php
// Heading
$_['heading_title']          = 'Услуги';

// Text
$_['text_success']           = 'Настройки успешно изменены!';
$_['text_list']              = 'Список услуг';
$_['text_add']               = 'Добавить';
$_['text_edit']              = 'Редактирование';
$_['text_default']           = 'Основной магазин';
$_['text_keyword']           = 'Должно быть уникальным на всю систему и без пробелов.';

// Column
$_['column_name']            = 'Услуги';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Услуга';
$_['entry_description']      = 'Описание';
$_['entry_meta_title'] 	     = 'Мета-тег Title';
$_['entry_meta_keyword']     = 'Мета-тег Keywords';
$_['entry_meta_description'] = 'Мета-тег Description';
$_['entry_store']            = 'Магазины';
$_['entry_keyword']          = 'SEO URL';
$_['entry_parent']           = 'Родительская услуга';
$_['entry_filter']           = 'Фильтры';
$_['entry_image']            = 'Изображение услуги';
$_['entry_top']              = 'Главное меню';
$_['entry_column']           = 'Столбцы';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_status']           = 'Статус';
$_['entry_layout']           = 'Макет';

// Help
$_['help_filter']            = '(Автозаполнение)';
$_['help_top']               = 'Показывать в главном меню (только для главных родительских услуг).';
$_['help_column']            = 'Количество столбцов в выпадающем меню Услуги (только для главных родительских услуг)';

// Error
$_['error_warning']          = 'Внимательно проверьте форму на ошибки!';
$_['error_permission']       = 'У Вас нет прав для изменения услуг!';
$_['error_name']             = 'Название Услуги должно быть от 1 до 255 символов!';
$_['error_meta_title']       = 'Ключевое слово должно быть от 1 до 255 символов!';
$_['error_keyword']          = 'SEO URL занят!';
$_['error_unique']           = 'SEO URL должен быть уникальным!';
$_['error_parent']           = 'Родительская услуга выбрана неправильно!';

