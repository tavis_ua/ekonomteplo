<?php

class ControllerCatalogSticker extends Controller
{
    private $error = array();

    public function index()
    {

        $data = [];

        $data['heading_title'] = "Стікери";

        $this->load->model('catalog/sticker');
        $this->load->language('catalog/sticker');

        $data['breadcrumbs'][] = array(
            'text' => 'Стікери',
            'href' => $this->url->link('catalog/sticker', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['error_warning'] = '';
        $data['column_name'] = $this->language->get('column_name');
        $data['column_action'] = $this->language->get('column_action');
        $data['help_filename'] = $this->language->get('help_filename');
        $data['entry_filename'] = $this->language->get('entry_filename');
        $data['text_loading'] = $this->language->get('text_loading');
        $data['button_upload'] = $this->language->get('button_upload');
        $data['error_filename'] = [];

        $data['user_token'] = $this->session->data['user_token'];


        $data['text_form'] = $this->language->get('text_form');
        $data['success'] = '';
        $data['text_list'] = $this->language->get('text_list');
        $sticker = $this->model_catalog_sticker->getStickers();
        $data['attributes'] = [];
        foreach ($sticker as $key => $value) {
            $data['attributes'][] = [
                'id' => $value['attribute_img_id'],
                'name' => $value['name'],
                'img' => $value['img'],
                'edit' => $this->url->link('catalog/sticker/edit', 'user_token=' . $this->session->data['user_token'] . '&attribute_img_id=' . $value['attribute_img_id'], true),
                'delete' => $this->url->link('catalog/sticker/delete', 'user_token=' . $this->session->data['user_token'] . '&attribute_img_id=' . $value['attribute_img_id'], true)
            ];
        }

        $data['add'] = $this->url->link('catalog/sticker/add', 'user_token=' . $this->session->data['user_token'], true);


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        $this->response->setOutput($this->load->view('catalog/sticker_list', $data));
    }

    public function delete()
    {
        $this->load->model('catalog/sticker');
        $this->model_catalog_sticker->deleteSticker($this->request->get['attribute_img_id']);
        $this->response->redirect($this->url->link('catalog/sticker', 'user_token=' . $this->session->data['user_token'], true));
    }


    public function add()
    {
        $this->load->language('catalog/sticker');
        $this->load->model('catalog/sticker');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $this->model_catalog_sticker->addSticker($this->request->post);
            $this->response->redirect($this->url->link('catalog/sticker', 'user_token=' . $this->session->data['user_token'], true));
        }
        $data = [];



        $data['button_save'] = $this->language->get('button_save');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['action'] = $this->url->link('catalog/sticker/add', 'user_token=' . $this->session->data['user_token'], true);

        $data['breadcrumbs'][] = array(
            'text' => 'Стікери',
            'href' => $this->url->link('catalog/sticker', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['entry_filename'] = $this->language->get('entry_filename');

        $data['text_loading'] = $this->language->get('text_loading');

        $data['button_upload'] = $this->language->get('button_upload');
        $data['error_filename'] = [];

        $data['user_token'] = $this->session->data['user_token'];

        $data['filename'] = $this->language->get('filename');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['text_attribute'] = $this->language->get('text_attribute');

        $data['filename'] = $this->language->get('filename');

        $data['text_form'] = $this->language->get('text_form');
        $data['action'] = $this->url->link('catalog/sticker/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['error_warning'] = false;


        $attributes = $this->model_catalog_sticker->getAttributes();

        $data['attributes'] = [];
        foreach ($attributes as $key => $value) {
            $data['attributes'][] = [
                'attributes_id' =>$value['attributes_id'],
                'value'         => $value['TEXT'],
                'name'          => $value['name'] . ": " . $value['TEXT'],
            ];
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/sticker_form', $data));
    }

    public function upload()
    {
        $this->load->language('catalog/download');

        $json = array();

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'catalog/download')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (!$json) {
            if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
                // Sanitize the filename
                $filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

                // Validate the filename length
                if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
                    $json['error'] = $this->language->get('error_filename');
                }

                // Allowed file extension types
                $allowed = array();

                $extension_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_ext_allowed'));

                $filetypes = explode("\n", $extension_allowed);

                foreach ($filetypes as $filetype) {
                    $allowed[] = trim($filetype);
                }

                if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Allowed file mime types
                $allowed = array();

                $mime_allowed = preg_replace('~\r?\n~', "\n", $this->config->get('config_file_mime_allowed'));

                $filetypes = explode("\n", $mime_allowed);

                foreach ($filetypes as $filetype) {
                    $allowed[] = trim($filetype);
                }

                if (!in_array($this->request->files['file']['type'], $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Check to see if any PHP files are trying to be uploaded
                $content = file_get_contents($this->request->files['file']['tmp_name']);

                if (preg_match('/\<\?php/i', $content)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Return any upload error
                if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
                    $json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
                }
            } else {
                $json['error'] = $this->language->get('error_upload');
            }
        }

        if (!$json) {
            $file = $filename;
            if (!file_exists(DIR_IMAGE . "/sticker/")) {
                mkdir(DIR_IMAGE . "/sticker/");
            }
            move_uploaded_file($this->request->files['file']['tmp_name'], DIR_IMAGE . "/sticker/" . $file);

            $json['filename'] = $file;
            $json['mask'] = $filename;

            $json['success'] = $this->language->get('text_upload');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}