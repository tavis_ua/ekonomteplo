<?php
class ModelCatalogSticker extends Model {
	public function addSticker($data) {
        $sql = "INSERT INTO " . DB_PREFIX . "attribute_img (name, img, descr) VALUES ('{$data['name']}', '" . $this->db->escape($data['filename']) . "', '" . $this->db->escape($data['descr']) . "')";

        $this->db->query($sql);

        $sticker_id = $this->db->getLastId();
		return $sticker_id;
	}

	public function editSticker($sticker_id, $data) {
        switch ($data['type_review']) {
            case 1:{
                $this->db->query("UPDATE " . DB_PREFIX . "review SET author = '" . $this->db->escape($data['author']) . "', product_id = '" . (int)$data['product_id'] . "', text = '" .
                    $this->db->escape(strip_tags($data['text'])) . "', rating = '" .
                    (int)$data['rating'] . "', status = '" . (int)$data['status'] . "', date_added = '" .
                    $this->db->escape($data['date_added']) . "', `positive_review` = '" . $this->db->escape(strip_tags($data['input_positive_review'])) . "', 
                    `negative_review` = '" . $this->db->escape(strip_tags($data['input_negative_review'])) . "', date_modified = NOW() WHERE review_id = '" . (int)$sticker_id . "'");
            }break;
            case 2:{
                $this->db->query("UPDATE " . DB_PREFIX . "post_review SET author = '" . $this->db->escape($data['author']) . "', post_id = '" . (int)$data['post_id'] . "', text = '" . $data['text'] .
                    "', rating = '" . (int)$data['rating'] . "', status = '" . (int)$data['status'] . "', date_added = '" . $this->db->escape($data['date_added']) . "', `positive_review` = '" .
                    $this->db->escape(strip_tags($data['input_positive_review'])) . "', 
                    `negative_review` = '" . $this->db->escape(strip_tags($data['input_negative_review'])) . "', date_modified = NOW() WHERE post_review_id = '" . (int)$sticker_id . "'");
            }break;
        }

		$this->cache->delete('product');
	}

	public function deleteSticker($sticker_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "attribute_img WHERE attribute_img_id=" . $sticker_id);
        return true;
	}

	public function getSticker($sticker_id) {

	}

	public function getAttributes(){
	    $sql = "SELECT GROUP_CONCAT(DISTINCT " . DB_PREFIX . "product_attribute.attribute_id) attributes_id, " . DB_PREFIX . "product_attribute.TEXT, " . DB_PREFIX . "attribute_description.name
                FROM " . DB_PREFIX . "product_attribute,
                     " . DB_PREFIX . "product,
                     " . DB_PREFIX . "attribute_description
                WHERE 1
                  AND " . DB_PREFIX . "attribute_description.attribute_id = " . DB_PREFIX . "product_attribute.attribute_id
                  AND " . DB_PREFIX . "product_attribute.product_id = " . DB_PREFIX . "product.product_id
                  AND " . DB_PREFIX . "product.`status` = 1
                group by " . DB_PREFIX . "product_attribute.TEXT, " . DB_PREFIX . "attribute_description.name";
	    $query = $this->db->query($sql);
	    return $query->rows;
    }
	public function getStickers() {
	    $test = true;
		$sql = "SELECT * FROM " . DB_PREFIX . "attribute_img";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalStickers($data = array()) {
		$sql = "SELECT COUNT(*) total from 
        (SELECT COUNT(*) as total from " . DB_PREFIX . "post_review
        UNION ALL
        SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r 
		LEFT JOIN " . DB_PREFIX . "product_description pd ON (r.product_id = pd.product_id) 
		WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
        $sql .= ") calc";
		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalStickersAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review WHERE status = '0'");

		return $query->row['total'];
	}
}