<?php
class ModelCatalogService extends Model {
	public function addService($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "service SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$service_id = $this->db->getLastId();

		foreach ($data['service_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "service_description SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$level = 0;

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

		foreach ($query->rows as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "service_path` SET `service_id` = '" . (int)$service_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

			$level++;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "service_path` SET `service_id` = '" . (int)$service_id . "', `path_id` = '" . (int)$service_id . "', `level` = '" . (int)$level . "'");


		$this->cache->delete('service');

		return $service_id;
	}

	public function editService($service_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "service SET parent_id = '" . (int)$data['parent_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE service_id = '" . (int)$service_id . "'");


		$this->db->query("DELETE FROM " . DB_PREFIX . "service_description WHERE service_id = '" . (int)$service_id . "'");

		foreach ($data['service_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "service_description SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "service_path` WHERE path_id = '" . (int)$service_id . "' ORDER BY level ASC");

		if ($query->rows) {
			foreach ($query->rows as $service_path) {
				// Delete the path below the current one
				$this->db->query("DELETE FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$service_path['service_id'] . "' AND level < '" . (int)$service_path['level'] . "'");

				$path = array();

				// Get the nodes new parents
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Get whats left of the nodes current path
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$service_path['service_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Combine the paths with a new level
				$level = 0;

				foreach ($path as $path_id) {
					$this->db->query("REPLACE INTO `" . DB_PREFIX . "service_path` SET service_id = '" . (int)$service_path['service_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

					$level++;
				}
			}
		} else {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$service_id . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "service_path` SET service_id = '" . (int)$service_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "service_path` SET service_id = '" . (int)$service_id . "', `path_id` = '" . (int)$service_id . "', level = '" . (int)$level . "'");
		}

		$this->cache->delete('service');
	}

	public function deleteService($service_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "service_path WHERE service_id = '" . (int)$service_id . "'");

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_path WHERE path_id = '" . (int)$service_id . "'");

		foreach ($query->rows as $result) {
			$this->deleteService($result['service_id']);
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "service WHERE service_id = '" . (int)$service_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "service_description WHERE service_id = '" . (int)$service_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "post_to_service WHERE service_id = '" . (int)$service_id . "'");
		$this->cache->delete('service');
	}

	public function repairServices($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service WHERE parent_id = '" . (int)$parent_id . "'");

		foreach ($query->rows as $service) {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$service['service_id'] . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "service_path` WHERE service_id = '" . (int)$parent_id . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "service_path` SET service_id = '" . (int)$service['service_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "service_path` SET service_id = '" . (int)$service['service_id'] . "', `path_id` = '" . (int)$service['service_id'] . "', level = '" . (int)$level . "'");

			$this->repairServices($service['service_id']);
		}
	}

	public function getService($service_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "service_path cp LEFT JOIN " . DB_PREFIX . "service_description cd1 ON (cp.path_id = cd1.service_id AND cp.service_id != cp.path_id) WHERE cp.service_id = c.service_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.service_id) AS path FROM " . DB_PREFIX . "service c LEFT JOIN " . DB_PREFIX . "service_description cd2 ON (c.service_id = cd2.service_id) WHERE c.service_id = '" . (int)$service_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row;
	}

	public function getServices($data = array()) {
		$sql = "SELECT cp.service_id AS service_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "service_path cp LEFT JOIN " . DB_PREFIX . "service c1 ON (cp.service_id = c1.service_id) LEFT JOIN " . DB_PREFIX . "service c2 ON (cp.path_id = c2.service_id) LEFT JOIN " . DB_PREFIX . "service_description cd1 ON (cp.path_id = cd1.service_id) LEFT JOIN " . DB_PREFIX . "service_description cd2 ON (cp.service_id = cd2.service_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.service_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getServiceDescriptions($service_id) {
		$service_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_description WHERE service_id = '" . (int)$service_id . "'");

		foreach ($query->rows as $result) {
			$service_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description']
			);
		}

		return $service_description_data;
	}
	
	public function getServicePath($service_id) {
		$query = $this->db->query("SELECT service_id, path_id, level FROM " . DB_PREFIX . "service_path WHERE service_id = '" . (int)$service_id . "'");

		return $query->rows;
	}

	public function getTotalServices() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "service");

		return $query->row['total'];
	}

}