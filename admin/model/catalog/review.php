<?php
class ModelCatalogReview extends Model {
	public function addReview($data) {
        switch ($data['type_review']) {
            case 1:{
                 $this->db->query("INSERT INTO " . DB_PREFIX . "review SET author = '" .
                     $this->db->escape($data['author']) . "', product_id = '" . (int)$data['product_id'] . "', text = '" .
                     $this->db->escape(strip_tags($data['text'])) . "', rating = '" . (int)$data['rating'] . "', status = '" .
                     (int)$data['status'] . "', date_added = '" . $this->db->escape($data['date_added']) . "', `positive_review` = '" .
                     $this->db->escape(strip_tags($data['input_positive_review'])) . "', `negative_review` = '" .
                     $this->db->escape(strip_tags($data['input_negative_review'])) . "'");
                 $this->cache->delete('product');
            }break;
            case 2:{
                $this->db->query("INSERT INTO " . DB_PREFIX . "post_review SET author = '" . $this->db->escape($data['author']) .
                    "', post_id = '" . (int)$data['post_id'] . "', text = '" . $data['text'] . "', rating = '" .
                    (int)$data['rating'] . "', status = '" . (int)$data['status'] . "', date_added = '" .
                    $this->db->escape($data['date_added']) . "', `positive_review` = '" .
                    $this->db->escape(strip_tags($data['input_positive_review'])) . "', `negative_review` = '" .
                    $this->db->escape(strip_tags($data['input_negative_review'])) . "'");
            }break;
        }
        $review_id = $this->db->getLastId();
		return $review_id;
	}

	public function editReview($review_id, $data) {
        switch ($data['type_review']) {
            case 1:{
                $this->db->query("UPDATE " . DB_PREFIX . "review SET author = '" . $this->db->escape($data['author']) . "', product_id = '" . (int)$data['product_id'] . "', text = '" .
                    $this->db->escape(strip_tags($data['text'])) . "', rating = '" .
                    (int)$data['rating'] . "', status = '" . (int)$data['status'] . "', date_added = '" .
                    $this->db->escape($data['date_added']) . "', `positive_review` = '" . $this->db->escape(strip_tags($data['input_positive_review'])) . "', 
                    `negative_review` = '" . $this->db->escape(strip_tags($data['input_negative_review'])) . "', date_modified = NOW() WHERE review_id = '" . (int)$review_id . "'");
            }break;
            case 2:{
                $this->db->query("UPDATE " . DB_PREFIX . "post_review SET author = '" . $this->db->escape($data['author']) . "', post_id = '" . (int)$data['post_id'] . "', text = '" . $data['text'] .
                    "', rating = '" . (int)$data['rating'] . "', status = '" . (int)$data['status'] . "', date_added = '" . $this->db->escape($data['date_added']) . "', `positive_review` = '" .
                    $this->db->escape(strip_tags($data['input_positive_review'])) . "', 
                    `negative_review` = '" . $this->db->escape(strip_tags($data['input_negative_review'])) . "', date_modified = NOW() WHERE post_review_id = '" . (int)$review_id . "'");
            }break;
        }

		$this->cache->delete('product');
	}

	public function deleteReview($review_list) {
	    foreach ($review_list as $type_review => $review_list) {
	        switch ($type_review){
                case 1:{
                    foreach ($review_list as $review_id) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE review_id = '" . (int)$review_id . "'");
                    }
                }break;
                case 2:{
                    foreach ($review_list as $review_id) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "post_review WHERE post_review_id = '" . (int)$review_id . "'");
                    }
                }break;
            }
        }
        $this->cache->delete('product');
	}

	public function getReview($type, $review_id) {
	    switch ($type) {
            case 1:{
                $query = $this->db->query("SELECT DISTINCT *, 
                (SELECT pd.name FROM " . DB_PREFIX . "product_description pd 
                WHERE pd.product_id = r.product_id AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS product 
                FROM " . DB_PREFIX . "review r WHERE r.review_id = '" . (int)$review_id . "'");
            }break;
            case 2:{
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_review WHERE post_review_id = '" . (int)$review_id . "'");
            }break;
        }
		return $query->row;
	}

	public function getReviews($data = array()) {
		$sql = "
            SELECT pr.post_review_id, opd.name, pr.author, pr.rating, pr.status, pr.date_added, 2 as type
            FROM " . DB_PREFIX . "post_review pr
            LEFT JOIN " . DB_PREFIX . "post p ON (p.post_id = pr.post_id)
            LEFT JOIN " . DB_PREFIX . "post_description opd on p.post_id = opd.post_id            
            UNION ALL
            SELECT r.review_id, pd.name, r.author, r.rating, r.status, r.date_added, 1 as type 
            FROM " . DB_PREFIX . "review r 
            LEFT JOIN " . DB_PREFIX . "product_description pd ON (r.product_id = pd.product_id) 
            WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$sort_data = array(
			'pd.name',
			'r.author',
			'r.rating',
			'r.status',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalReviews($data = array()) {
		$sql = "SELECT COUNT(*) total from 
        (SELECT COUNT(*) as total from " . DB_PREFIX . "post_review
        UNION ALL
        SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r 
		LEFT JOIN " . DB_PREFIX . "product_description pd ON (r.product_id = pd.product_id) 
		WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
        $sql .= ") calc";
		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalReviewsAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review WHERE status = '0'");

		return $query->row['total'];
	}
}