<?php
class ModelDesignBanner extends Model {
	public function addBanner($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "banner SET background_image = '" . $data['background_image'] . "', name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "'");

		$banner_id = $this->db->getLastId();

		if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $language_id => $value) {
				foreach ($value as $banner_image) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "banner_image SET banner_id = '" . (int)$banner_id . "', language_id = '" . (int)$language_id . "', title = '" .  $this->db->escape($banner_image['title']) . "', link = '" .  $this->db->escape($banner_image['link']) . "', image = '" .  $this->db->escape($banner_image['image']) . "', sort_order = '" .  (int)$banner_image['sort_order'] . "'");
				}
			}
		}

		return $banner_id;
	}

	public function editBanner($banner_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "banner SET background_image = '" . $data['background_image'] . "', name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "' WHERE banner_id = '" . (int)$banner_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_html WHERE banner_id = '" . (int)$banner_id . "'");

		if (isset($data['banner_image'])) {
            $index = 0;
			foreach ($data['banner_image'] as $language_id => $value) {
				foreach ($value as $key => $banner_image) {
                    $key_index = array_keys($data['type_content'][$index])[0];
				    switch ($data['type_content'][$index][$key_index]) {
                        case 1:
                        {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "banner_image SET banner_id = '" . (int)$banner_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($banner_image['title']) . "', link = '" . $this->db->escape($banner_image['link']) . "', image = '" . $this->db->escape($banner_image['image']) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'");
                        }break;
                        case 2:{
                            $sql = "INSERT INTO " . DB_PREFIX . "banner_html SET banner_id = '" . (int)$banner_id . "', title = '" . $this->db->escape($banner_image['title']) . "', html = '" . str_replace( "'", "\'", $data['html-content-'.str_replace("'", '', $index)]) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'";
                            $this->db->query($sql);
                        }
                    }
                    $index++;
				}
			}
		}
	}

	public function deleteBanner($banner_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_html WHERE banner_id = '" . (int)$banner_id . "'");
	}

	public function getBanner($banner_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "banner WHERE banner_id = '" . (int)$banner_id . "'");

		return $query->row;
	}

	public function getBanners($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "banner";

		$sort_data = array(
			'name',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getBannerImages($banner_id, $table_prefix = '') {
		$banner_image_data = array();

		if(!empty($table_prefix)){
            $table_prefix .= '_';
        }

		$banner_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_prefix . "banner_image WHERE ". (empty($table_prefix) ? 'banner_' : $table_prefix) ."id = '" . (int)$banner_id . "' ORDER BY sort_order ASC");
        $language_id = isset($banner_image['language_id']) ? $banner_image['language_id'] : $this->config->get('config_language_id');

		foreach ($banner_image_query->rows as $banner_image) {
			$banner_image_data[$language_id][] = array(
				'title'      => $banner_image['title'],
				'link'       => $banner_image['link'],
				'image'      => $banner_image['image'],
				'sort_order' => $banner_image['sort_order']
			);
		}

		return $banner_image_data;
	}

	public function getBannerHTML($banner_id, $table_prefix = ''){
	    $banner_html_data = [];
        if(!empty($table_prefix)){
            $table_prefix .= '_';
        }
	    $banner_html_query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_prefix . "banner_html WHERE ". (empty($table_prefix) ? 'banner_' : $table_prefix) ."id = '" . (int)$banner_id . "' ORDER BY sort_order ASC");

	    foreach ($banner_html_query->rows as $banner_html) {
            $banner_html_data[] = array(
                'title'      => $banner_html['title'],
                'html'       => $banner_html['html'],
                'sort_order' => $banner_html['sort_order']
            );
        }

        return $banner_html_data;
    }

	public function getTotalBanners() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "banner");

		return $query->row['total'];
	}
}
