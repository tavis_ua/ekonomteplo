<?php
class ModelDesignMenu extends Model {
    public function getMenuByName($name) {
        return $this->getMenus([
            'filter' => [
                'name' => $name
            ]
        ]);
    }

	public function addMenu($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "menu` SET status = 1");
		$menu_id = $this->db->getLastId();
        $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_description` SET menu_id = {$menu_id}, `name`='{$data['name']}', language_id = '" . (int)$data['language_id'] . "'");
		foreach ($data['menu_item'] as $menu_item){
            if(!empty($menu_item['menu_item_name'])) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_items` SET sort_order = " . (int)$menu_item['menu_item_sort_order'] . ",  menu_id = {$menu_id},`menu_item_title` = '" . $this->db->escape($menu_item['menu_item_name']) . "', `menu_item_url` = '" . $this->db->escape($menu_item['menu_item_link']) . "'");
            }
        }
	}

	public function editMenu($menu_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "menu_description` SET `name`='{$data['name']}', language_id = '" . (int)$data['language_id'] . "' WHERE menu_id = '" . (int)$menu_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "menu_items` WHERE menu_id= '" . (int)$menu_id . "'");
        foreach ($data['menu_item'] as $menu_item){
            if(!empty($menu_item['menu_item_name'])) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_items` SET sort_order = " . (int)$menu_item['menu_item_sort_order'] . ",  menu_id = {$menu_id},`menu_item_title` = '" . $this->db->escape($menu_item['menu_item_name']) . "', `menu_item_url` = '" . $this->db->escape($menu_item['menu_item_link']) . "'");
            }
        }
	}

	public function deleteMenu($menu_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "menu_module` WHERE menu_id = '" . (int)$menu_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "menu_items` WHERE menu_id = '" . (int)$menu_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "menu_description` WHERE menu_id = '" . (int)$menu_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "menu` WHERE menu_id = '" . (int)$menu_id . "'");
		return true;
	}
	
	public function getMenu($menu_id, $language_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_description` WHERE menu_id = '" . (int)$menu_id . "' and `language_id` = ".$language_id);
		$data = [
		    'menu_id' => $menu_id,
		    'name' => $query->row['name']
        ];
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_items` WHERE menu_id = '" . (int)$menu_id . "' order by sort_order");
		foreach ($query->rows as $menu_item){
		    $data['menu_items'][]= [
		        'title' => $menu_item['menu_item_title'],
		        'url' => $menu_item['menu_item_url'],
		        'sort_order' => $menu_item['sort_order'],
            ];
        }

		return $data;
	}

	public function getMenus($data = array()) {
		$sql = "select * from oc_menu
            left join oc_menu_description omd on oc_menu.menu_id = omd.menu_id where 1";

		if(isset($data['filter'])) {
		    foreach ($data['filter'] as $key => $filter) {
		        $sql .= " AND `{$key}` = '{$filter}'";
            }
        }

     	$sort_data = array(
			'name',
			'language_id',
			'store_id'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY omd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalMenu($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "menu`";
		
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}