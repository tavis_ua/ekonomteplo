<?php
class ModelCommonPost extends Model {

    public function getTotalCategories() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post_category");

        return $query->row['total'];
    }

    public function editCategory($category_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "post_category SET without_link = '" . (int)isset($data['without_link']) . "', parent_id = '" . (int)$data['parent_id'] . "', `image` = '" . $this->db->escape($data['image']) . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE post_category_id = '" . (int)$category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "post_category SET image = '" . $this->db->escape($data['image']) . "' WHERE post_category_id = '" . (int)$category_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_description WHERE post_category_id = '" . (int)$category_id . "'");

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "post_category_description SET post_category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "post_category_path` WHERE post_category_id = '" . (int)$category_path['post_category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_category_path` WHERE post_category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_category_path` WHERE post_category_id = '" . (int)$category_path['post_category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "post_category_path` SET post_category_id = '" . (int)$category_path['post_category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "post_category_path` WHEREpost_category_id = '" . (int)$category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_category_path` WHERE post_category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "post_category_path` SET post_category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "post_category_path` SET post_category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_filter WHERE post_category_id = '" . (int)$category_id . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "post_category_filter SET post_category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_to_store WHERE post_category_id = '" . (int)$category_id . "'");

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "post_category_to_store SET post_category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        // SEO URL
        $this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'post_category_id=" . (int)$category_id . "'");

        if (isset($data['category_seo_url'])) {
            foreach ($data['category_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'post_category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_to_layout WHERE post_category_id = '" . (int)$category_id . "'");

        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "post_category_to_layout SET post_category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('category');
    }

    public function addCategory($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "post_category SET without_link = '" . (int)isset($data['without_link']) . "', `image` = '" . $this->db->escape($data['image']) . "', parent_id = '" . (int)$data['parent_id'] .
            "', status = '" . (int)$data['status'] . "', top = '" . (isset($data['top']) ? (int)$data['top'] : 0)  ."', sort_order = '" . (int)$data['sort_order'] ."', date_modified = NOW(), date_added = NOW()");

        $post_category_id = $this->db->getLastId();

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "post_category_description SET post_category_id = '" . (int)$post_category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_category_path` WHERE post_category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "post_category_path` SET `post_category_id` = '" . (int)$post_category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "post_category_path` SET `post_category_id` = '" . (int)$post_category_id . "', `path_id` = '" . (int)$post_category_id . "', `level` = '" . (int)$level . "'");


        // SEO URL

        if (isset($data['category_seo_url'])) {
            foreach ($data['category_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'post_category_id=" . (int)$post_category_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }

        // Set which layout to use with this category
        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "post_category_to_layout SET post_category_id = '" . (int)$post_category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('category');

        return $post_category_id;
    }

    public function getCategoryStores($category_id) {
        $category_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_category_to_store WHERE post_category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_store_data[] = $result['store_id'];
        }

        return $category_store_data;
    }

    public function getCategory($post_category_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "post_category_path cp LEFT JOIN " . DB_PREFIX . "post_category_description cd1 ON (cp.path_id = cd1.post_category_id AND cp.post_category_id != cp.path_id) WHERE cp.post_category_id = c.post_category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.post_category_id) AS path FROM " . DB_PREFIX . "post_category c LEFT JOIN " . DB_PREFIX . "post_category_description cd2 ON (c.post_category_id = cd2.post_category_id) WHERE c.post_category_id = '" . (int)$post_category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getService($service_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(sd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "service_path sp LEFT JOIN " . DB_PREFIX . "service_description sd1 ON (sp.path_id = sd1.service_id AND sp.service_id != sp.path_id) WHERE sp.service_id = s.service_id AND sd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY sp.service_id) AS path FROM " . DB_PREFIX . "service s LEFT JOIN " . DB_PREFIX . "service_description sd2 ON (s.service_id = sd2.service_id) WHERE s.service_id = '" . (int)$service_id . "' AND sd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function deleteCategory($post_category_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "post set post_category_id = null WHERE post_category_id = '" . (int)$post_category_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_path WHERE post_category_id = '" . (int)$post_category_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_category_path WHERE path_id = '" . (int)$post_category_id . "'");

        foreach ($query->rows as $result) {
            $this->deleteCategory($result['post_category_id']);
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category WHERE post_category_id = '" . (int)$post_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_description WHERE post_category_id = '" . (int)$post_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_filter WHERE post_category_id = '" . (int)$post_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_to_store WHERE post_category_id = '" . (int)$post_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "post_category_to_layout WHERE post_category_id = '" . (int)$post_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'post_category_id=" . (int)$post_category_id . "'");

        $this->cache->delete('category');
    }
    
    public function getCategoryDescriptions($post_category_id) {
        $category_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_category_description WHERE post_category_id = '" . (int)$post_category_id . "'");

        foreach ($query->rows as $result) {
            $category_description_data[$result['language_id']] = array(
                'name'             => $result['name'],
            );
        }

        return $category_description_data;
    }    
    
    public function getCategories($data = array()) {
        $sql = "SELECT cp.post_category_id AS post_category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "post_category_path cp LEFT JOIN " . DB_PREFIX . "post_category c1 ON (cp.post_category_id = c1.post_category_id) LEFT JOIN " . DB_PREFIX . "post_category c2 ON (cp.path_id = c2.post_category_id) LEFT JOIN " . DB_PREFIX . "post_category_description cd1 ON (cp.path_id = cd1.post_category_id) LEFT JOIN " . DB_PREFIX . "post_category_description cd2 ON (cp.post_category_id = cd2.post_category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";


        $sql .= " GROUP BY cp.post_category_id";

        $query = $this->db->query($sql);

        return $query->rows;
    }
    
	public function addPost($data) {

	    $sql = "INSERT INTO " . DB_PREFIX . "post SET post_type = '" . (int)$data['post_type']."'";
	    if(isset($data['sort_order'])){
	        $sql .= "', is_submenu = '" . (isset($data['is_submenu']) ? 1 : 0) . "', sort_order = '" . (int)$data['sort_order'] . "'";
        }
        $sql .= ", date_added = NOW(), date_modified = NOW()";
		$this->db->query($sql);

		$post_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "post SET image = '" . $this->db->escape($data['image']) . "' WHERE post_id = '" . (int)$post_id . "'");
		}

		foreach ($data['post_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "post_description SET post_id = '" . (int)$post_id . "', language_id = '" . (int)$language_id . "', 
			name = '" . $this->db->escape($value['name']) . "', sub_name = '" . $this->db->escape($value['sub_name']) . "', description = '" . $this->db->escape($value['description']) . "',
			text = '" . $this->db->escape($value['text']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['post_store'])) {
			foreach ($data['post_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_to_store SET post_id = '" . (int)$post_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['post_attribute'])) {
            $attributes = [];
			foreach ($data['post_attribute'] as $post_attribute) {

                foreach ($post_attribute['post_attribute_description'] as $language_id => $post_attribute_description) {
                    if (array_search($post_attribute['name'], $attributes)) {
                        // Removes duplicates
                        $this->db->query("DELETE FROM " . DB_PREFIX . "post_attribute WHERE attribute_id = '" . array_search($post_attribute['name'], $attributes) ."'" );
                    }

                    $this->db->query("INSERT INTO " . DB_PREFIX . "post_attribute SET post_id = '" . (int)$post_id . "', `name` = '" . $post_attribute['name'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($post_attribute_description['text']) . "'");
                    $attribute_id = $this->db->getLastId();
                    $attributes[$attribute_id] = $post_attribute['name'];
                }

			}
		}

		if (isset($data['post_option'])) {
			foreach ($data['post_option'] as $post_option) {
				if ($post_option['type'] == 'select' || $post_option['type'] == 'radio' || $post_option['type'] == 'checkbox' || $post_option['type'] == 'image') {
					if (isset($post_option['post_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "post_option SET post_id = '" . (int)$post_id . "', option_id = '" . (int)$post_option['option_id'] . "', required = '" . (int)$post_option['required'] . "'");

						$post_option_id = $this->db->getLastId();

						foreach ($post_option['post_option_value'] as $post_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "post_option_value SET post_option_id = '" . (int)$post_option_id . "', post_id = '" . (int)$post_id . "', option_id = '" . (int)$post_option['option_id'] . "', option_value_id = '" . (int)$post_option_value['option_value_id'] . "', quantity = '" . (int)$post_option_value['quantity'] . "', subtract = '" . (int)$post_option_value['subtract'] . "', price = '" . (float)$post_option_value['price'] . "', price_prefix = '" . $this->db->escape($post_option_value['price_prefix']) . "', points = '" . (int)$post_option_value['points'] . "', points_prefix = '" . $this->db->escape($post_option_value['points_prefix']) . "', weight = '" . (float)$post_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($post_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "post_option SET post_id = '" . (int)$post_id . "', option_id = '" . (int)$post_option['option_id'] . "', value = '" . $this->db->escape($post_option['value']) . "', required = '" . (int)$post_option['required'] . "'");
				}
			}
		}

		if (isset($data['post_recurring'])) {
			foreach ($data['post_recurring'] as $recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "post_recurring` SET `post_id` = " . (int)$post_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
			}
		}

		if (isset($data['post_publish_period'])) {
			foreach ($data['post_publish_period'] as $post_publish_period) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_publish_period SET post_id = '" . (int)$post_id . "', customer_group_id = '" . (int)$post_publish_period['customer_group_id'] . "', priority = '" . (int)$post_publish_period['priority'] . "', date_start = '" . $this->db->escape($post_publish_period['date_start']) . "', date_end = '" . $this->db->escape($post_publish_period['date_end']) . "'");
			}
		}

		if (isset($data['post_image'])) {
			foreach ($data['post_image'] as $post_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_image SET post_id = '" . (int)$post_id . "', image = '" . $this->db->escape($post_image['image']) . "', sort_order = '" . (int)$post_image['sort_order'] . "'");
			}
		}

		if (isset($data['post_download'])) {
			foreach ($data['post_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_to_download SET post_id = '" . (int)$post_id . "', download_id = '" . (int)$download_id . "'");
			}
		}

		if (isset($data['post_category'])) {
			foreach ($data['post_category'] as $post_category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_to_category SET post_id = '" . (int)$post_id . "', post_category_id = '" . (int)$post_category_id . "'");
			}
		}
		
		if (isset($data['post_service'])) {
			foreach ($data['post_service'] as $post_service_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_to_service SET post_id = '" . (int)$post_id . "', post_service_id = '" . (int)$post_service_id . "'");
			}
		}
		
		if (isset($data['post_filter'])) {
			foreach ($data['post_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_filter SET post_id = '" . (int)$post_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		if (isset($data['post_related']) && is_array($data['post_related'])) {
			foreach ($data['post_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$post_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_related SET post_id = '" . (int)$post_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$related_id . "' AND related_id = '" . (int)$post_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_related SET post_id = '" . (int)$related_id . "', related_id = '" . (int)$post_id . "'");
			}
		}


		// SEO URL
		if (isset($data['post_seo_url'])) {
			foreach ($data['post_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'post_id=" . (int)$post_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		$this->cache->delete('post');

		return $post_id;
	}

	public function editPost($post_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "post SET  is_submenu = '" . (isset($data['is_submenu']) ? 1 : 0) . "', post_type = '".(int) $data['post_type']."', date_modified = NOW() WHERE post_id = '" . (int)$post_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "post SET image = '" . $this->db->escape($data['image']) . "' WHERE post_id = '" . (int)$post_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "post_description WHERE post_id = '" . (int)$post_id . "'");

		foreach ($data['post_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "post_description SET post_id = '" . (int)$post_id . "', language_id = '" . (int)$language_id .
                "', name = '" . $this->db->escape($value['name']) . "', sub_name = '" . $this->db->escape($value['sub_name']) . "', description = '" . $this->db->escape($value['description']) .
                "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) .
                "', text = '" . $this->db->escape($value['text']) .
                "',meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "post_to_store WHERE post_id = '" . (int)$post_id . "'");

		if (isset($data['post_store'])) {
			foreach ($data['post_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_to_store SET post_id = '" . (int)$post_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

        $this->db->query("DELETE FROM ". DB_PREFIX . "post_related WHERE post_id = '" . (int)$post_id . "'");

		if(!empty($data['post_related']) && is_array($data['post_related'])){
            foreach ($data['post_related'] as $post_related){
                // Removes duplicates
                $this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$post_id . "' AND related_id = '" . (int)$post_related . "'");

                $this->db->query("INSERT INTO " . DB_PREFIX . "post_related SET post_id = '" . (int)$post_id . "', related_id = '" . (int)$post_related . "'");

            }
        }

		$this->db->query("DELETE FROM ". DB_PREFIX . "post_properties WHERE post_id = '" . (int)$post_id . "'");

		$this->db->query("INSERT INTO ".DB_PREFIX . "post_properties SET post_id = '" . (int)$post_id . "', properties = '" . $this->db->escape($data['post_properties']) . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "post_attribute WHERE post_id = '" . (int)$post_id . "'");

		if (!empty($data['post_attribute'])) {
			foreach ($data['post_attribute'] as $post_attribute) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "post_attribute WHERE post_id = " . (int)$post_id . " AND attribute_id = '" . (empty($post_attribute['attribute_id'])? 0 : (int)$post_attribute['attribute_id']) . "'");
                foreach ($post_attribute['post_attribute_description'] as $language_id => $post_attribute_description) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "post_attribute SET post_id={$post_id}, text = '{$post_attribute_description['text']}', language_id = '{$language_id}', name = '{$post_attribute['name']}'");
                }
			}
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "post_recurring` WHERE post_id = " . (int)$post_id);

		if (isset($data['post_recurring'])) {
			foreach ($data['post_recurring'] as $post_recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "post_recurring` SET `post_id` = " . (int)$post_id . ", customer_group_id = " . (int)$post_recurring['customer_group_id'] . ", `recurring_id` = " . (int)$post_recurring['recurring_id']);
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_publish_period WHERE post_id = '" . (int)$post_id . "'");

		if (isset($data['post_publish_period']) && is_array($data['post_publish_period'])) {
			foreach ($data['post_publish_period'] as $post_publish_period) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_publish_period SET post_id = '" . (int)$post_id .
                    "', customer_group_id = '" . (int)$post_publish_period['customer_group_id'] .
                    "', priority = '" . (int)$post_publish_period['priority'] .
                    "', date_start = '" . $this->db->escape($post_publish_period['date_start']) .
                    "', date_end = '" . $this->db->escape($post_publish_period['date_end']) . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "post_image WHERE post_id = '" . (int)$post_id . "'");

		if (isset($data['post_image'])) {
			foreach ($data['post_image'] as $post_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_image SET post_id = '" . (int)$post_id . "', image = '" . $this->db->escape($post_image['image']) . "', sort_order = '" . (int)$post_image['sort_order'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "post_to_category WHERE post_id = '" . (int)$post_id . "'");

		if (isset($data['post_category'])) {
			foreach ($data['post_category'] as $post_category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_to_category SET post_id = '" . (int)$post_id . "', category_id = '" . (int)$post_category_id . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_to_service WHERE post_id = '" . (int)$post_id . "'");

		if (isset($data['post_service'])) {
			foreach ($data['post_service'] as $post_service_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_to_service SET post_id = '" . (int)$post_id . "', service_id = '" . (int)$post_service_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "post_filter WHERE post_id = '" . (int)$post_id . "'");

		if (isset($data['post_filter'])) {
			foreach ($data['post_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_filter SET post_id = '" . (int)$post_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE related_id = '" . (int)$post_id . "'");

		if (isset($data['post_related']) && is_array($data['post_related'])) {
			foreach ($data['post_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$post_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_related SET post_id = '" . (int)$post_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$related_id . "' AND related_id = '" . (int)$post_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "post_related SET post_id = '" . (int)$related_id . "', related_id = '" . (int)$post_id . "'");
			}
		}

		// SEO URL
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'post_id=" . (int)$post_id . "'");
		
		if (isset($data['post_seo_url']) && is_array($data['post_seo_url'])) {
			foreach ($data['post_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'post_id=" . (int)$post_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		// POST PRODUCT
        $this->db->query("DELETE FROM " . DB_PREFIX . "post_product WHERE post_id = " . $post_id );
        if(isset($data['post_product'])){
            foreach ($data['post_product'] as $item){
                $this->db->query("INSERT INTO " . DB_PREFIX . "post_product SET post_id = " . $post_id . ", product_id = " . $item['product_id'] . ", count = '" . $item['count']. "'");
            }
        }

		$this->cache->delete('post');
	}

    public function getCategorySeoUrls($category_id) {
        $category_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'post_category_id=" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $category_seo_url_data;
    }

	public function copyPost($post_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "post p WHERE p.post_id = '" . (int)$post_id . "'");

		if ($query->num_rows) {
			$data = $query->row;

			$data['sku'] = '';
			$data['upc'] = '';
			$data['viewed'] = '0';
			$data['keyword'] = '';
			$data['status'] = '0';

			$data['post_attribute'] = $this->getpostAttributes($post_id);
			$data['post_description'] = $this->getpostDescriptions($post_id);
			$data['post_filter'] = $this->getpostFilters($post_id);
			$data['post_image'] = $this->getpostImages($post_id);
			$data['post_related'] = $this->getpostRelated($post_id);
			$data['post_publish_period'] = $this->getpostSpecials($post_id);
			$data['post_category'] = $this->getpostCategories($post_id);
			$data['post_store'] = $this->getpostStores($post_id);
			$data['post_recurrings'] = $this->getRecurrings($post_id);

			$this->addPost($data);
		}
	}

	public function deletePost($post_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "post WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_attribute WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_description WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_image WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_related WHERE related_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_publish_period WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_to_category WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_to_service WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_to_store WHERE post_id = '" . (int)$post_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "post_recurring WHERE post_id = " . (int)$post_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'post_id=" . (int)$post_id . "'");
		$this->cache->delete('post');
	}

	public function getPost($post_id) {
		$query = $this->db->query("SELECT DISTINCT pd.*, pp.*, p.* FROM " . DB_PREFIX . "post p 
		    LEFT JOIN " . DB_PREFIX . "post_description pd ON (p.post_id = pd.post_id) 
		    LEFT JOIN " . DB_PREFIX . "post_properties pp ON (p.post_id = pp.post_id) 
		    WHERE p.post_id = '" . (int)$post_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		return $query->row;
	}


    public function getPosts($data = array()) {
        $sql = "SELECT pb.*, pd.*, p.* FROM " . DB_PREFIX . "post p 
        LEFT JOIN " . DB_PREFIX . "post_description pd ON (p.post_id = pd.post_id)
        LEFT JOIN " . DB_PREFIX . "post_publish_period pb ON (p.post_id = pb.post_id) 
        WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY p.post_id";

        $sort_data = array(
            'pd.name',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getPostProducts($post_id){
        $query = $this->db->query("SELECT opd.product_id, language_id, name, description, tag, meta_title, meta_description, meta_keyword, meta_h1, oc_post_product.count from oc_post_product
                LEFT JOIN oc_product on oc_product.product_id = oc_post_product.product_id
                LEFT JOIN oc_product_description opd on oc_post_product.product_id = opd.product_id
                WHERE oc_post_product.post_id = " . $post_id . " AND opd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        return $query->rows;
    }

	public function getPostsByCategoryId($post_category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post p LEFT JOIN " . DB_PREFIX . "post_description pd ON (p.post_id = pd.post_id) LEFT JOIN " . DB_PREFIX . "post_to_category p2c ON (p.post_id = p2c.post_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.post_category_id = '" . (int)$post_category_id . "' ORDER BY pd.name ASC");

		return $query->rows;
	}

	public function getPostDescriptions($post_id) {
		$post_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_description WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'sub_name'         => $result['sub_name'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'tag'              => $result['tag'],
				'text'             => $result['text'],
			);
		}

		return $post_description_data;
	}

	public function getPostCategories($post_id) {
		$post_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_to_category WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_category_data[] = $result['category_id'];
		}

		return $post_category_data;
	}
	
	public function getPostServices($post_id) {
		$post_service_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_to_service WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_service_data[] = $result['service_id'];
		}

		return $post_service_data;
	}

	public function getPostFilters($post_id) {
		$post_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_filter WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_filter_data[] = $result['filter_id'];
		}

		return $post_filter_data;
	}

	public function getPostAttributes($post_id) {
		$post_attribute_data = array();

		$post_attribute_query = $this->db->query("SELECT attribute_id, name FROM " . DB_PREFIX . "post_attribute WHERE post_id = '" . (int)$post_id . "' GROUP BY attribute_id");

		foreach ($post_attribute_query->rows as $post_attribute) {
			$post_attribute_description_data = array();

			$post_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_attribute WHERE post_id = '" . (int)$post_id . "' AND attribute_id = '" . (int)$post_attribute['attribute_id'] . "'");

			foreach ($post_attribute_description_query->rows as $post_attribute_description) {
				$post_attribute_description_data[$post_attribute_description['language_id']] = array('text' => $post_attribute_description['text']);
			}

			$post_attribute_data[] = array(
				'attribute_id'                  => $post_attribute['attribute_id'],
				'name'                          => $post_attribute['name'],
				'post_attribute_description'    => $post_attribute_description_data
			);
		}

		return $post_attribute_data;
	}

	public function getPostOptions($post_id) {
		$post_option_data = array();

		$post_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.post_id = '" . (int)$post_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		foreach ($post_option_query->rows as $post_option) {
			$post_option_value_data = array();

			$post_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON(pov.option_value_id = ov.option_value_id) WHERE pov.post_option_id = '" . (int)$post_option['post_option_id'] . "' ORDER BY ov.sort_order ASC");

			foreach ($post_option_value_query->rows as $post_option_value) {
				$post_option_value_data[] = array(
					'post_option_value_id' => $post_option_value['post_option_value_id'],
					'option_value_id'         => $post_option_value['option_value_id'],
					'quantity'                => $post_option_value['quantity'],
					'subtract'                => $post_option_value['subtract'],
					'price'                   => $post_option_value['price'],
					'price_prefix'            => $post_option_value['price_prefix'],
					'points'                  => $post_option_value['points'],
					'points_prefix'           => $post_option_value['points_prefix'],
					'weight'                  => $post_option_value['weight'],
					'weight_prefix'           => $post_option_value['weight_prefix']
				);
			}

			$post_option_data[] = array(
				'post_option_id'    => $post_option['post_option_id'],
				'post_option_value' => $post_option_value_data,
				'option_id'            => $post_option['option_id'],
				'name'                 => $post_option['name'],
				'type'                 => $post_option['type'],
				'value'                => $post_option['value'],
				'required'             => $post_option['required']
			);
		}

		return $post_option_data;
	}

	public function getPostOptionValue($post_id, $post_option_value_id) {
		$query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "post_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.post_id = '" . (int)$post_id . "' AND pov.post_option_value_id = '" . (int)$post_option_value_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getPostImages($post_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_image WHERE post_id = '" . (int)$post_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getPostDiscounts($post_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_discount WHERE post_id = '" . (int)$post_id . "' ORDER BY quantity, priority, price");

		return $query->rows;
	}

	public function getPostPublishPeriods($post_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_publish_period WHERE post_id = '" . (int)$post_id . "' ORDER BY priority");

		return $query->rows;
	}

	public function getPostRewards($post_id) {
		$post_reward_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_reward WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
		}

		return $post_reward_data;
	}

	public function getPostDownloads($post_id) {
		$post_download_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_to_download WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_download_data[] = $result['download_id'];
		}

		return $post_download_data;
	}

	public function getPostStores($post_id) {
		$post_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_to_store WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_store_data[] = $result['store_id'];
		}

		return $post_store_data;
	}
	
	public function getPostSeoUrls($post_id) {
		$post_seo_url_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'post_id=" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
		}

		return $post_seo_url_data;
	}
	
	public function getPostLayouts($post_id) {
		$post_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_to_layout WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $post_layout_data;
	}

	public function getPostRelated($post_id) {
		$post_related_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_related WHERE post_id = '" . (int)$post_id . "'");

		foreach ($query->rows as $result) {
			$post_related_data[] = $result['related_id'];
		}

		return $post_related_data;
	}

	public function getRecurrings($post_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_recurring` WHERE post_id = '" . (int)$post_id . "'");

		return $query->rows;
	}

	public function getTotalPosts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.post_id) AS total FROM " . DB_PREFIX . "post p LEFT JOIN " . DB_PREFIX . "post_description pd ON (p.post_id = pd.post_id)";

		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalPostsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post WHERE stock_status_id = '" . (int)$stock_status_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByLengthClassId($length_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post WHERE length_class_id = '" . (int)$length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post_to_download WHERE download_id = '" . (int)$download_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByManufacturerId($manufacturer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByAttributeId($attribute_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByOptionId($option_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post_option WHERE option_id = '" . (int)$option_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByProfileId($recurring_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post_recurring WHERE recurring_id = '" . (int)$recurring_id . "'");

		return $query->row['total'];
	}

	public function getTotalPostsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
}
