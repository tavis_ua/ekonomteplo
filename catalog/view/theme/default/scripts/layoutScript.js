const orderCall = document.querySelector('.order_call');
const orderCallInputs = document.querySelector('.order_call-inputs');
const searchBTN = document.querySelector('.search-btn');
const search = document.querySelector('.search');
const searchClose = document.querySelector('.search-close');
const OrderCall = document.querySelector('.order_call');
const TopHeaderLine = document.querySelector('.top-header-line');
const MidleHeaderLine = document.querySelector('.midle-header-line');
const BottomHeaderLine = document.querySelector('.bottom-header-line');
const BottomHeaderLineNav = document.querySelector('.bottom-header-line nav');
const CallHeader = document.querySelector('.call-header');
const Header = document.querySelector('header');
const HeaderClouser = document.querySelector('.header-clouser');

let orderCallCond = true;
let searchCallCond = true;

orderCall.addEventListener('click', () => {
  if (orderCallCond) {
    console.log(screen.width );
    if(screen.width > 767) {
      orderCallInputs.style.top = '220px';
    }else{
      orderCallInputs.style.top = '70vh';
    }
    orderCallCond = false;
  }else {
    orderCallInputs.style.top = '-400px';
    orderCallCond = true;
  }
});

searchBTN.addEventListener('click', searchCall );
searchClose.addEventListener('click', searchCall );

function searchCall() {
  if (searchCallCond) {
    search.style.right = '-3.8%';
    searchCallCond = false;
  }else {
    search.style.right = '-100vw';
    searchCallCond = true;
  }
}

CallHeader.addEventListener('click', ()=>{
  OrderCall.style.display = 'flex';
  TopHeaderLine.style.display = 'flex';
  MidleHeaderLine.style.display = 'flex';
  BottomHeaderLineNav.style.display = 'flex';
  BottomHeaderLine.style.marginTop = '13vw';
  BottomHeaderLine.style.height = 'unset';
  BottomHeaderLine.style.alignItems = 'unset';
  BottomHeaderLine.style.justifyContent = 'unset';
  BottomHeaderLine.style.background = 'unset';
  Header.style.position = 'sticky';
  CallHeader.style.display = 'none';
})

HeaderClouser.addEventListener('click', ()=>{
  OrderCall.style.display = 'none';
  TopHeaderLine.style.display = 'none';
  MidleHeaderLine.style.display = 'none';
  BottomHeaderLineNav.style.display = 'none';
  BottomHeaderLine.style.marginTop = '0';
  BottomHeaderLine.style.height = '12.5vw';
  BottomHeaderLine.style.alignItems = 'flex-end';
  BottomHeaderLine.style.justifyContent = 'center';
  BottomHeaderLine.style.background = '#E53A40';
  Header.style.position = 'fixed';
  CallHeader.style.display = 'flex';
})