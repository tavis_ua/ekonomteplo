const slide = document.querySelectorAll('.slider-realized-current img');
const left = document.querySelector('.arrow-left');
const right = document.querySelector('.arrow-right');
const slideItems = document.querySelector('.slides');
let curS = 0;;
var arrayElem = [];

for (var i = 0; i <= slide.length-1 ; i++) {
	const slideItem = document.createElement('span');
	slideItems.appendChild(slideItem);
}

const slideItemsSelect = document.querySelectorAll('.slides span');
right.addEventListener('click',()=>{
	for (var i = 0; i <= slide.length -1; i++) {
		slide[i].style.display = 'none';
		slideItemsSelect[i].classList.remove('active');
	}
	curS++;
	if (curS > slide.length -1) {
		curS = 0;
	}
	slide[curS].style.display = 'block';
	slideItemsSelect[curS].classList.add('active');
})	

left.addEventListener('click',()=>{
	for (var i = 0; i <= slide.length -1; i++) {
		slide[i].style.display = 'none';
		slideItemsSelect[i].classList.remove('active');
	}
	curS--;
	if (curS < 0) {
		curS = slide.length -1;
	}
	slide[curS].style.display = 'block';
	slideItemsSelect[curS].classList.add('active');
})

for (var i = 0; i < slideItemsSelect.length; i++){
  	arrayElem.push(slideItemsSelect[i]);
  	slideItemsSelect[i].addEventListener('click', function(e){
  		for (var w = 0; w <= slide.length -1; w++) {
			slide[w].style.display = 'none';
			slideItemsSelect[w].classList.remove('active');
		}
     	curS = arrayElem.indexOf(this);
     	slide[curS].style.display = 'block';
     	slideItemsSelect[curS].classList.add('active');
  	});
}


slide[0].style.display = 'block';
slideItemsSelect[0].classList.add('active');
