<?php

class ControllerPostPost extends Controller {

    public function index(){
        $this->load->language('post/post');

        $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
        $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
        $this->document->addStyle('catalog/view/theme/'. $this->config->get('config_theme') . '/stylesheet/post_review_form.css');
        $this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

        $data['header'] = $this->load->controller('common/header');
        $this->load->language('post_review_form');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->load->model('catalog/post');

        $post_info = $this->model_catalog_post->getPost($this->request->get['post_id']);
        $data['breadcrumbs'] = [];
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if(isset($this->request->get['_route_'])) {
            $url = isset($this->request->get['_route_']);
        } else {
            $url = 'post/post';
        }

        $data['breadcrumbs'][] = array(
            'text' => $post_info['name'],
            'href' => $this->url->link('/'.$url, (isset($this->session->data['user_token']) ? 'user_token=' . $this->session->data['user_token'] : ''), true)
        );

        $data['post_id']        = $post_info['post_id'];
        $data['post_type']      = $post_info['post_type'];
        $data['post_name']      = $post_info['name'];
        $data['image']          = $post_info['image'];
        $data['images']         = $this->model_catalog_post->getImages($this->request->get['post_id']);
        $data['properties']     = html_entity_decode($post_info['properties']);
        $data['text']           = html_entity_decode ($post_info['description']);
        $data['products']       = $post_info['products'];
        $data['content_top']    = $this->load->controller('common/content_top');
        $data['reviews']        = $this->language->get('reviews');

        $data['translate'] = [
            'exelent'        =>  $this->language->get('exelent'),
            'very_good'      =>  $this->language->get('very_good'),
            'good'           =>  $this->language->get('good'),
            'bad'            =>  $this->language->get('bad'),
            'very_bad'       =>  $this->language->get('very_bad'),
            'rate_title'     =>  $this->language->get('rate_title'),
            'rate_comment'   =>  $this->language->get('rate_comment'),
            'submit'         =>  $this->language->get('submit'),
            'name'           =>  $this->language->get('name'),
            'age'            =>  $this->language->get('age'),
            'email'          =>  $this->language->get('email'),
            'phone'          =>  $this->language->get('phone'),
        ];
        $data['attributes']     = $this->model_catalog_post->getAttributes($this->request->get['post_id']);

        $rating = 0;
        if(isset($post_info['reviews']) && !empty($post_info['reviews'])) {
            foreach ($post_info['reviews'] as $review) {
                $rating += $review['rating'];
            }
            $rating = floor($rating / count($post_info['reviews']));
            $data['rating'] = (int)$rating;
            $data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$post_info['reviews']);
        }
        $data['products_title'] = $this->language->get('products_title');
        $data['text'] = html_entity_decode($post_info['text']);

        //Аналогичные объекты
        $data['related_posts'] = [];
        foreach ($this->model_catalog_post->getRelatedPosts($this->request->get['post_id']) as $post_id){
            $data['related_posts'][] = $this->model_catalog_post->getPost($post_id['post_id']);
        }

        foreach ($data['images'] as $image) {
            if (is_file(DIR_IMAGE . $image['image'])) {
                $data['banners'][] = array(
                    'title' => $post_info['name'],
                    'link'  => '',
//                    'image' => $this->model_tool_image->resize($image['image'], 1280, 250)
                    'image' => 'image/'.$image['image']
                );
            }
        }
        $data['review_form'] = [
            'review_form_title' => $this->language->get('form_review_title'),
            'rate_our_services' => $this->language->get('rate_our_services'),
            'your_personal_info' => $this->language->get('your_personal_info'),
            'write_your_feedback' => $this->language->get('write_your_feedback'),
            'send_message' => $this->language->get('send_message'),
        ];
        switch ($data['post_type']) {
            case 2:{
                $this->response->setOutput($this->load->view('post/services', $data));
            }break;
            default:
            {
                if(isset($data['banners']) && !empty($data['banners'])) {
                    $setting = [
                        'banners' => $data['banners'],
                        'module' => 0,
                        'addition_classes' => ['posts-slider'],
                        'slidesPreView' => 1
                    ];
                    $data['banners'] = $this->load->controller('extension/module/slideshow', $setting);
                }else{
                    $data['banners'] = '';
                }
                $this->response->setOutput($this->load->view('post/post', $data));
            }break;
        }
    }

    public function review() {
        $this->load->language('post/post');

        $this->load->model('catalog/post');
        $this->load->language('post_review_form');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['reviews'] = array();

        $review_total = $this->model_catalog_post->getTotalReviewsByPostId($this->request->get['post_id']);

        $results = $this->model_catalog_post->getReviewsByPostId($this->request->get['post_id'], ($page - 1) * 5, 5);

        foreach ($results as $result) {
            $data['reviews'][] = array(
                'author'     => $result['author'],
                'text'       => html_entity_decode($result['text']),
                'rating'     => (int)$result['rating'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $pagination = new Pagination();
        $pagination->total = $review_total;
        $pagination->page = $page;
        $pagination->limit = 5;
        $pagination->url = $this->url->link('post/post/review', 'product_id=' . $this->request->get['post_id'] . '&page={page}');

        $data['pagination'] = $pagination->render();

        $data['translate'] = [
            'exelent'        =>  $this->language->get('exelent'),
            'very_good'      =>  $this->language->get('very_good'),
            'good'           =>  $this->language->get('good'),
            'bad'            =>  $this->language->get('bad'),
            'very_bad'       =>  $this->language->get('very_bad'),
            'rate_title'     =>  $this->language->get('rate_title'),
            'rate_comment'   =>  $this->language->get('rate_comment'),
            'submit'         =>  $this->language->get('submit'),
            'name'           =>  $this->language->get('name'),
            'age'            =>  $this->language->get('age'),
            'email'          =>  $this->language->get('email'),
            'phone'          =>  $this->language->get('phone'),
        ];

        $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

        $this->response->setOutput($this->load->view('product/review', $data));
    }
}