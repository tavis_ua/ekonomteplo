<?php


class ControllerPostCategory extends Controller {

    public function index(){
//        if(!isset($this->request->get['post_category_id'])){
//            $this->response->redirect($this->url->link('common/home'));
//        }
        $this->load->language('post/post');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->load->model('catalog/post');
        $data['breadcrumbs'] = [];
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if(isset($this->request->get['post_category_id'])) {
            $category_info = $this->model_catalog_post->getCategory($this->request->get['post_category_id']);
            $data['breadcrumbs'][] = array(
                'text' => $category_info['name'],
                'href' => $_SERVER['REQUEST_URI'],
            );
            $data['heading_title'] = $category_info['name'];
            $data['post_category_id'] = $this->request->get['post_category_id'];
            $data['posts'] = $this->model_catalog_post->getPotsCategory($this->request->get['post_category_id']);
        }
        $data['search_by_product'] = $this->language->get('search_by_product');
        $this->response->setOutput($this->load->view('post/category', $data));
    }

    public function getPosts(){
        $this->load->model('catalog/post');
        $data['posts'] = $this->model_catalog_post->getPotsCategory([
            'post_category_id' => $this->request->get['post_category_id'],
            'product_id' => isset($this->request->get['product_id']) ? (int) $this->request->get['product_id'] : 0,
        ]);
        $out = $this->load->view('post/post_list', $data);
        exit($out);
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/post');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort'        => 'name',
                'order'       => 'ASC',
                'start'       => 0,
                'limit'       => 5
            );

            $results = $this->model_catalog_post->getCategories($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'post_category_id' => $result['post_category_id'],
                    'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}