<?php
class ControllerExtensionModuleProducts extends Controller {
	public function index() {
		$this->load->language('extension/module/products');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (!empty($parts)) {
			$data['category_id'] = array_pop($parts);
		} else {
			$data['category_id'] = 0;
		}

		$this->load->model('catalog/product');


		$data['heading_title'] = $this->language->get('heading_title');

		if(!empty($data['category_id'])) {
            $filter_params['filter_category_id'] = $data['category_id'];
        }

		if(isset($this->request->get['limit'])){
            $limit = '&limit=' . $this->request->get['limit'];
        }else{
            $limit = '&limit=15';
        }
        if (isset($this->request->get['mfp'])){
            $mfp = '&mfp=' . $this->request->get['mfp'];
        }else{
            $mfp = '';
        }
        $route = isset($this->request->get['_route_']) ? $this->request->get['_route_'] : $this->request->get['route'];
		$data['sort_types'] = [
            'sort_default' => ['url' => '/'. $route .'&sort=default&order=' . $limit . $mfp, 'param_name' => 'default', 'sort_title' => $this->language->get('sort_default'), ],
            'sort_name_ASC' => ['url' => '/'. $route .'&sort=name&order=ASC' . $limit . $mfp, 'param_name' => 'name', 'sort_title' => $this->language->get('sort_name_asc'), ],
            'sort_name_DESC' => ['url' => '/'. $route .'&sort=name&order=DESC' . $limit . $mfp, 'param_name' => 'name', 'sort_title' => $this->language->get('sort_name_desc'), ],
            'sort_price_ASC' => ['url' => '/'. $route .'&sort=price&order=ASC' . $limit . $mfp, 'param_name' => 'price', 'sort_title' => $this->language->get('sort_price_asc'), ],
            'sort_price_DESC' => ['url' => '/'. $route .'&sort=price&order=DESC' . $limit . $mfp, 'param_name' => 'price', 'sort_title' => $this->language->get('sort_price_desc'), ],
            'sort_rating_DESC' => ['url' => '/'. $route .'&sort=rating&order=ASC' . $limit . $mfp, 'param_name' => 'rating', 'sort_title' => $this->language->get('sort_rating_desc'), ],
            'sort_rating_ASC' => ['url' => '/'. $route .'&sort=rating&order=DESC' . $limit . $mfp, 'param_name' => 'rating', 'sort_title' => $this->language->get('sort_rating_asc'), ],
            'sort_model_ASC' => ['url' => '/'. $route .'&sort=model&order=ASC' . $limit . $mfp, 'param_name' => 'model', 'sort_title' => $this->language->get('sort_model_asc'), ],
            'sort_model_DESC' => ['url' => '/'. $route .'&sort=model&order=DESC' . $limit . $mfp, 'param_name' => 'model', 'sort_title' => $this->language->get('sort_model_desc'), ],
        ];

        $filter_params = [
            'limit' => isset($_GET['limit']) ? $_GET['limit'] : 15,
//            'limit' => 8,
            'sort' => 'p.price',
            'start' => (isset($this->request->get['page']) ? $this->request->get['page'] : 0 ) * (isset($_GET['limit']) ? $_GET['limit'] : 15)
        ];
        if(isset($data['category_id']) && !empty($data['category_id'])){
            $filter_params['filter_category_id'] = $data['category_id'];
        };
		if(isset($this->request->get['sort'])){
		    $sort="&sort=" . $this->request->get['sort']. "&order=" .$this->request->get['order'];
            $data['sort_types']['sort_' . $this->request->get['sort'] . '_' . $this->request->get['order']]['selected'] = 'selected';
            $filter_params['sort'] = 'p.' . $this->request->get['sort'];
            $filter_params['order'] = $this->request->get['order'];
        }else{
            $sort = '';
        }


		$data['limits'] =[
            '15' => ['url' => '/'. $route .'?limit=15' .  $sort . $mfp, 'limit_title' => 15, ],
            '25' => ['url' => '/'. $route . '?limit=25' . $sort . $mfp, 'limit_title' => 25, ],
            '50' => ['url' => '/'. $route . '?limit=50' . $sort . $mfp, 'limit_title' => 50, ],
            '75' => ['url' => '/'. $route . '?limit=75' . $sort . $mfp, 'limit_title' => 75, ],
            '100' => ['url' => '/'. $route . '?limit=100' . $sort . $mfp, 'limit_title' => 100, ],
        ];
		if(isset($this->request->get['limit'])){
		    $data['limits'][$this->request->get['limit']]['selected'] = 'selected';
        }

		$products = $this->model_catalog_product->getProducts($filter_params);

		foreach ($products as $product) {

            $attributes = $this->model_catalog_product->getProductAttributes($product['product_id']);
			$data['products'][] = array(
				'product_id' => $product['product_id'],
				'name'        => $product['name'],
				'stickers'    => $product['stickers'],
				'image'       => $product['image'],
				'special_price' => (int)$product['special'],
				'price'       => $this->currency->format($this->tax->calculate((int)$product['price'], 0, $this->config->get('config_tax')), $this->session->data['currency']),
				'href'        => isset($product['keyword']) && !empty($product['keyword']) ? $product['keyword'] : $this->url->link('product/product', 'product_id=' . $product['product_id']),
                'attributes'  => !empty($attributes) ? $attributes[0]['attribute']:[]
			);
		}
        $data['products']['hide_action'] = isset($this->request->get['ajax']) ? $this->request->get['ajax'] : false;
        $data['products']['show_title'] = isset($this->request->get['page']) ? $this->request->get['page'] : 0 == 0;
        $data['products']['count_per_page'] = isset($this->request->get['limit']) ? $this->request->get['limit'] : false;

        if (isset($this->request->get['ajax'])){
            $data['hide_action'] = true;
        }
        $html = $this->load->view('extension/module/products', $data);
        if (isset($this->request->get['ajax'])){
            $out = ['success' => true, 'html' => $html];
            exit(json_encode($out));
        }
		return $html;
	}
}