<?php
class ControllerExtensionModuleBanner extends Controller {
	public function index($setting) {
	    if(empty($setting)){
	        return '';
        }
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (isset($result['image']) && is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}elseif ( isset($result['html']) ){
                $data['banners'][] = array(
                    'title' => $result['title'],
                    'html'  => html_entity_decode($result['html']),
                );
            }
		}
        $data['width'] = $setting['width'];
        $data['height'] = $setting['height'];
		$data['module'] = $module++;
        if(is_numeric($setting['banner_id'])) {
            return $this->load->view('extension/module/banner', $data);
        }elseif ($setting['banner_id'] == 'catalog_banner'){
            return $this->load->view('extension/module/catalog_banner', $data);
        }
	}
}