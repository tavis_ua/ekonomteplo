<?php
class ControllerExtensionModulePost extends Controller {
	private $error = array();

	public function index($setting) {
	    if(empty($setting)){
	        return '';
        }
        $this->load->language('extension/module/post');
        $this->load->model('extension/module/post');
        $posts = $this->model_extension_module_post->getPosts($setting['post_type']);
        $post_list_id = [];
        foreach ($posts as $key => $post){
            $post_list_id[] = $post['post_id'];
            $posts[$key]['description'] = html_entity_decode($post['description'], ENT_QUOTES, 'UTF-8');
            $url_list = $this->model_extension_module_post->getPostSeoUrls($post['post_id']);
            if($url_list) {
                $posts[$key]['url'] = $url_list[$this->config->get('config_store_id')][$this->config->get('config_language_id')];
            }else{
                $user_token = '';
                if(isset($this->session->data['user_token'])) {
                    $user_token = 'user_token=' . $this->session->data['user_token'];
                }
                $posts[$key]['url'] = $this->url->link('post/post&post_id='.$post['post_id'], $user_token, true);
            }
        }

        $attributes = $this->model_extension_module_post->getAttributes($post_list_id);
        foreach ($attributes as $attribute){
            $posts[$attribute['post_id']]['attribute'][] = $attribute;
        }

        $images = $this->model_extension_module_post->getImages($post_list_id);
        foreach ($images as $image){
            $posts[$attribute['post_id']]['images'][] = $image;
        }

        $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
        $data['posts'] = $posts;
        $data['head_title'] = $setting['name'];

        $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
        $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
        $this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

		switch ($setting['post_type']){
            case 2:{
                $services = $this->model_extension_module_post->getServiceStructure();
                foreach (@$services as $key => $service){
                    if(isset($service['description'])){
                        @$services[$key]['description'] = htmlspecialchars_decode($service['description']);
                    }
                    $subservice = $this->model_extension_module_post->getServiceStructure($service['service_id']);
                    if(!empty($subservice)) {
                        foreach ($subservice as $subitem) {
                            @$services[] = $subitem;
                        }
                    }
                }
                $data['services'] = $services;
                return $this->load->view('extension/module/post/services', $data);
            }break;
            case 5:{
                return $this->load->view('extension/module/post/advantage_post', $data);
            }break;
            default:{

            }
        }
	}
}
