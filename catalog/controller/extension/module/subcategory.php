<?php
class ControllerExtensionModuleSubCategory extends Controller {
	public function index() {
		$this->load->language('extension/module/category');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

        if (!empty($parts)) {
            $data['category_id'] = array_pop($parts);
        } else {
            $data['category_id'] = 0;
        }

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();
		$data['heading_title'] = $this->language->get('heading_title');

		$categories = $this->model_catalog_category->getCategories($data['category_id']);

		foreach ($categories as $category) {
			$data['categories'][] = array(
				'category_id' => $category['category_id'],
				'name'        => $category['name'],
				'image'       => $category['image'],
				'href'        => (isset($category['keyword']) && !empty($category['keyword'])) ? '/'.$category['keyword'] : $this->url->link('product/category', 'path=' . $category['category_id'])
			);
		}

		return $this->load->view('extension/module/subcategory', $data);
	}

}