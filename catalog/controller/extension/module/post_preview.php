<?php


class ControllerExtensionModulePostPreview extends Controller
{
    public function index($setting) {
        if(empty($setting)){
            return '';
        }
        static $post_preview_module = 0;
        $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
        $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
        $this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');
        $data['preview_block_type'] = $setting['preview_block_type'];
        $this->load->model('extension/module/post_preview');
        $this->load->language('extension/module/post_preview');
        $data['post_type'] = $setting['post_type'];
        $data['heading_title'] = $setting['name'];
        $data['post_list'] = $this->model_extension_module_post_preview->getPosts($setting['post_type']);
        $data['module'] = $post_preview_module;
        $post_preview_module++;
        if(count($data['post_list']) == 0){
            return '';
        }
        $out = $this->load->view('extension/module/post_preview', $data);
        if ($data['preview_block_type'] == 1 && $setting['name'] == 'Відгуки'){
            $out .= $this->load->view('information/consultation2');
        }
        return $out;
    }
}