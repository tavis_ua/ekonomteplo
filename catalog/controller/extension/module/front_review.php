<?php


class ControllerExtensionModuleFrontReview extends Controller
{
    public function index($setting)
    {
        if(empty($setting)){
            return '';
        }
        static $front_review_module = 0;
        $data['preview_block_type'] = $setting['preview_block_type'];
        $data['post_type']          = $setting['post_type'];
        $data['banners'] = array();

        $this->load->language('extension/module/front_review');

        $this->load->model('extension/module/front_review');
        $reviews = $this->model_extension_module_front_review->getReviews($setting['post_type'], isset($setting['category_id']) ? $setting['category_id'] : 0);

        $out = '';

        if($setting['preview_block_type']  == 1) {

            $this->load->model('tool/image');

            $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
            $this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
            $this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

        }
        foreach ($reviews as $review) {
            if (is_file(DIR_IMAGE . $review['image'])) {
                $data['banners'][] = array(
                    'author' => $review['author'],
                    'title' => $review['title'],
                    'link' => $review['url'],
                    'rating' => $review['rating'],
                    'price' => isset($review['price']) ? (int)$review['price'] : null,
                    'html' => html_entity_decode($review['text']),
                    'positive_review' => html_entity_decode($review['positive_review']),
                    'negative_review' => html_entity_decode($review['negative_review']),
                    'image' => $this->model_tool_image->resize($review['image'], $setting['width'], $setting['height'])
                );
            }
        }

        $data['module'] = $front_review_module++;

        if($data['banners']) {
            $out = $this->load->view('extension/module/front_review', $data);
        }
        if ($data['preview_block_type'] == 1){
            $out .= $this->load->view('information/consultation2');
        }
        return  $out;
    }

}