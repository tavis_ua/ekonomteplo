<?php
class ControllerExtensionModuleSlideshow extends Controller {
	public function index($setting) {
	    if(empty($setting)){
	        return '';
        }

		static $module = 0;		

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');
		
		$data['banners'] = array();
        if(isset($setting['addition_classes'])) {
            $data['addition_classes'] = implode(' ', $setting['addition_classes']);
        }
        if (!isset($setting['banners'])) {
            $results = $this->model_design_banner->getBanner($setting['banner_id']);

            foreach ($results as $result) {
                if(isset($result['background_image']) && !isset($data['background_image']) || empty($data['background_image'])){
                    $data['background_image'] = $result['background_image'];
                }
                if (isset($result['image']) && is_file(DIR_IMAGE . $result['image'])) {
                    $data['banners'][] = array(
                        'title' => $result['title'],
                        'link' => $result['link'],
                        'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
                    );
                }
                if (!empty($result['html'])){
                    $data['banners'][] = array(
                        'title' => $result['title'],
                        'html'  => html_entity_decode($result['html']),
                    );
                }
            }

            $data['module'] = $module++;
        }else{
            $data['banners'] = $setting['banners'];
        }
        $data['addition-class'] = isset($setting['addition-class']) ? $setting['addition-class'] : '';
        $data['slidesPreView'] =  1;//isset($setting['slidesPreView']) ? $setting['slidesPreView'] : count($data['banners']);
        if(count($data['banners']) == 0){
            return '';
        }
        $out = $this->load->view('extension/module/slideshow', $data);
		return $out;
	}
}