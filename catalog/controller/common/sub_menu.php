<?php
class ControllerCommonSubMenu extends Controller {
	public function index() {
		$this->load->language('common/sub_menu');

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'image' => $child['image']
					);
                    if(empty($child['seo_url_id'])) {
                        $children_data[ count($children_data) - 1]['href'] = $this->url->link('product/category', 'path=' . $child['category_id']);
                    }else{
                        $children_data[ count($children_data) - 1]['href'] = '/'.$this->model_catalog_post->getSeoUrl($child['seo_url_id'])['keyword'];
                    }
				}

				// Level 1
				$data['categories'][] = array(
				    'category_id'       => $category['category_id'],
					'name'     => $category['name'],
					'image'    => !empty($category['image'])?$server.'/image/'.$category['image']:'',
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
				);
                if(empty($category['seo_url_id'])) {
                    $data['categories'][ count($data['categories']) - 1]['href'] = $this->url->link('product/category', 'path=' . $category['category_id']);
                }else{
                    $data['categories'][ count($data['categories']) - 1]['href'] = '/'.$this->model_catalog_post->getSeoUrl($child['seo_url_id'])['keyword'];
                }
			}
		}
        //Post categories
        $this->load->model('catalog/post');
        $categories = $this->model_catalog_post->getCategories(0);

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_post->getCategories($category['post_category_id']);

                foreach ($children as $child) {

                    $children_data[] = array(
                        'name'  => $child['name'],
                        'image' => $child['image']
                    );
                    if(empty($child['seo_url_id'])) {
                        $children_data[ count($children_data) - 1]['href'] = $this->url->link('post/category', 'post_category_id=' . $child['post_category_id']);
                    }else{
                        $children_data[ count($children_data) - 1]['href'] = '/'.$this->model_catalog_post->getSeoUrl($child['seo_url_id'])['keyword'];
                    }
                }

                $posts = $this->model_catalog_post->getPosts($category['post_category_id'], true);

                foreach ($posts as $post) {

                    $children_data[] = array(
                        'name'  => $post['name'],
                        'image' => $post['image']
                    );
                    $seo_keyword = $this->model_catalog_post->getSeoUrlByQuery('post_id=' . $post['post_id']);
                    if(empty($seo_keyword['keyword'])) {
                        $children_data[ count($children_data) - 1]['href'] = $this->url->link('post/category', 'post_id=' . $post['post_id']);
                    }else{
                        $children_data[ count($children_data) - 1]['href'] = '/'.$seo_keyword['keyword'];
                    }
                }

                // Level 1
                $data['categories'][] = array(
                    'post_category_id' => $category['post_category_id'],
                    'name'     => $category['name'],
                    'image'    => !empty($category['image'])?$server.'/image/'.$category['image']:'',
                    'children' => $children_data,
                    'column'   => $category['column'] ? $category['column'] : 1,
                );
                if(empty($category['seo_url_id'])) {
                    $data['categories'][ count($data['categories']) - 1]['href'] = $this->url->link('post/category', 'post_category_id=' . $category['post_category_id']);
                }else{
                    $seo_keyword = $this->model_catalog_post->getSeoUrl($child['seo_url_id']);
                    if(!empty($seo_keyword)) {
                        $data['categories'][count($data['categories']) - 1]['href'] = '/' . $seo_keyword['keyword'];
                    }else{
                        $data['categories'][ count($data['categories']) - 1]['href'] = $this->url->link('post/category', 'post_category_id=' . $category['post_category_id']);
                    }
                }
            }
        }

		return $this->load->view('common/sub_menu', $data);
	}
}
