<?php
class ControllerCommonMenu extends Controller {
	public function index() {
		$this->load->language('common/menu');

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				// Level 1
				$data['categories'][] = array(
				    'category_id' => $category['category_id'],
					'name'     => $category['name'],
					'image'    => !empty($category['image'])?$server.'/image/'.$category['image']:'',
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
				);
				if($category['without_link']){
                    $data['categories'][count($data['categories']) - 1]['href'] = "javascript:void(0);";
                }else {
                    if (empty($category['seo_url_id'])) {
                        $data['categories'][count($data['categories']) - 1]['href'] = $this->url->link('product/category', 'path=' . $category['category_id']);
                    } else {
                        $data['categories'][count($data['categories']) - 1]['href'] = '/' . $this->model_catalog_post->getSeoUrl($category['seo_url_id'])['keyword'];
                    }
                }
			}
		}
        //Post categories
        $this->load->model('catalog/post');
        $categories = $this->model_catalog_post->getCategories(0);

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 1
                $data['categories'][] = array(
                    'post_category_id' => $category['post_category_id'],
                    'name'     => $category['name'],
                    'image'    => !empty($category['image'])?$server.'image/'.trim($category['image']):'',
                    'column'   => $category['column'] ? $category['column'] : 1,
                );
                if($category['without_link']){
                    $data['categories'][count($data['categories']) - 1]['href'] = "javascript:void(0);";
                }else {
                    if (empty($category['seo_url_id'])) {
                        $data['categories'][count($data['categories']) - 1]['href'] = $this->url->link('post/category', 'path=' . $category['post_category_id']);
                    } else {
                        $data['categories'][count($data['categories']) - 1]['href'] = '/' . $this->model_catalog_post->getSeoUrl($category['seo_url_id'])['keyword'];
                    }
                }
            }
        }

		return $this->load->view('common/menu', $data);
	}
}
