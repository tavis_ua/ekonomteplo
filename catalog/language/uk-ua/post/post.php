<?php
$_['text_home'] = 'ЕкономТепло';
$_['completed_title'] = 'Реалізовані об\'єкти';
$_['products_title'] = 'Використане обладнання';
$_['text_reviews'] = 'Відгуки <span>(%s)</span>';
$_['reviews'] = 'Відгуки';
$_['form_review_title'] = 'Залиште будь ласка свій відгук';
$_['your_personal_info'] = 'Ваші контактні дані';
$_['rate_our_services'] = 'Ваша оцінка';
$_['write_your_feedback'] = 'Залиште свій відгук';
$_['send_message'] = 'Відправити';
$_['search_by_product'] = 'Знайти за товаром';