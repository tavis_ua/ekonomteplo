<?php
// Heading
$_['heading_title'] = 'Товари';

$_['empty_list'] = 'Немає записів';

//Sort
$_['sort_default'] = 'За замовчуванням';
$_['sort_name_asc'] = 'За Ім\'ям (A - Я)';
$_['sort_name_desc'] = 'За Ім\'ям (Я - A)';
$_['sort_price_asc'] = 'За Ціною (зростання)';
$_['sort_price_desc'] = 'За Ціною (зменшення)';
$_['sort_rating_desc'] = 'За Рейтингом (зменшення)';
$_['sort_rating_asc'] = 'За Рейтингом (зростання)';
$_['sort_model_asc'] = 'За Моделлю (A - Я)';
$_['sort_model_desc'] = 'За Моделлю (Я - A)';