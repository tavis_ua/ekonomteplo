<?php
class ModelDesignMenu extends Model {
    public function getMenus($data = array()) {
        $sql = "select * from oc_menu
            left join oc_menu_description omd on oc_menu.menu_id = omd.menu_id where 1";

        if(isset($data['filter'])) {
            foreach ($data['filter'] as $key => $filter) {
                $sql .= " AND `{$key}` = '{$filter}'";
            }
        }

        $sort_data = array(
            'name',
            'language_id',
            'store_id'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY omd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getMenu($menu_id, $language_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_description` WHERE menu_id = '" . (int)$menu_id . "' and `language_id` = ".$language_id);
        $data = [
            'menu_id' => $menu_id,
            'name' => $query->row['name']
        ];
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_items` WHERE menu_id = '" . (int)$menu_id . "' order by sort_order");
        foreach ($query->rows as $menu_item){
            $data['menu_items'][]= [
                'title' => $menu_item['menu_item_title'],
                'url' => $menu_item['menu_item_url'],
                'sort_order' => $menu_item['sort_order'],
            ];
        }

        return $data;
    }

}