<?php

class ModelDesignBanner extends Model
{
    public function getBanner($banner_id)
    {
        $out = [];
        if (!is_numeric($banner_id)) {
            if ($banner_id == 'catalog_banner') {
                if (isset($this->request->get['path'])) {
                    $category_id = $this->request->get['path'];
                    do {
                        $sql = "select title, image, link, sort_order from " . DB_PREFIX . "category_banner_image where category_id = " . $category_id;
                        $query = $this->db->query($sql);
                        if (empty($query->rows)) {
                            $sql = "select parent_id category_id from " . DB_PREFIX . "category where category_id = " . $category_id;
                            $query = $this->db->query($sql);
                            $go = !empty($query->rows);
                            $category_id = isset($query->row['category_id']) ? $query->row['category_id'] : 0;
                        } else {
                            $go = false;
                        }
                    } while ($go);
                    if ($query->rows) {
                        $out = $query->rows;
                    }

                    $category_id = $this->request->get['path'];
                    do {
                        $sql = "select title, html, sort_order from " . DB_PREFIX . "category_banner_html where category_id = " . $category_id;
                        $query = $this->db->query($sql);
                        if (empty($query->rows)) {
                            $sql = "select parent_id category_id from " . DB_PREFIX . "category where category_id = " . $category_id;
                            $query = $this->db->query($sql);
                            $go = !empty($query->rows);
                            $category_id = isset($query->row['category_id']) ? $query->row['category_id'] : 0;
                        } else {
                            $go = false;
                        }
                    } while ($go);
                    if ($query->rows) {
                        $out = array_merge($out, $query->rows);
                    }
                    return $out;
                } else {
                    return [];
                }
            }
        } else {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner b LEFT JOIN " . DB_PREFIX . "banner_image bi ON (b.banner_id = bi.banner_id) WHERE b.banner_id = '" . (int)$banner_id . "' AND b.status = '1' AND bi.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY bi.sort_order ASC");
            $sql = "SELECT * FROM " . DB_PREFIX . "banner b LEFT JOIN " . DB_PREFIX . "banner_html bh ON (b.banner_id = bh.banner_id) where b.banner_id = " . $banner_id;
            $query_html = $this->db->query($sql);
            foreach ($query_html->rows as $row){
                $query->rows[] = $row;
            }
        }
        return $query->rows;
    }
}
