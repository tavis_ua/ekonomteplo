<?php
class ModelExtensionModulePostPreview extends Model {

	public function getPosts($post_type) {

        $sql = "select p.*, pd.name, pd.description from " . DB_PREFIX . "post p
        LEFT JOIN  " . DB_PREFIX . "post_description pd on pd.post_id = p.post_id
        LEFT JOIN  " . DB_PREFIX . "post_publish_period ppp on ppp.post_id = p.post_id
        where p.post_type = " . $post_type;
        $sql .= " AND pd.language_id = " . (int)$this->config->get('config_language_id');

        //Если выборка статей - делаю выборку статей без указания срока отображения и
        //если текущая дата попадает в период отображения статей
        if($post_type == 1){
            $sql .= "   AND (ppp.date_start is null and ppp.date_end is null
               or (date(now()) between ppp.date_start and ppp.date_end)
               or (date(now()) >= ppp.date_start and ppp.date_end = '0000-00-00'))";
        }
        $sql .= " LIMIT 0, 5";
	    $query = $this->db->query($sql);

	    $out = [];
	    foreach ($query->rows as $row){
	        $query_attributes = $this->db->query("select trim(name) name, trim(text) text from " . DB_PREFIX . "post_attribute where post_id = ".$row['post_id']);
            $row['description'] = html_entity_decode($row['description']);
            $user_token = '';
            if(isset($this->session->data['user_token'])){
                $user_token = 'user_token=' . $this->session->data['user_token'];
            }
            $url = $this->url->link('post/post', $user_token . '&post_id='.$row['post_id'], true);
            $url_query = $this->db->query("select keyword from " . DB_PREFIX . "seo_url where query = 'post_id=".$row['post_id']."'");
            if($url_query->num_rows > 0){
                if ($query->row){
                    $url = '/'.$url_query->row['keyword'];
                }
            }
            $row['url'] = $url;
            if(count($query_attributes->rows)) {
                $row['attributes'] = $query_attributes->rows;
            }
	        $out[$row['post_id']] = $row;
        }
		return $out;
	}

	public function getAttributes($posts_id){
        $out = [];
        if(is_array($posts_id) && count($posts_id) > 0) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_attribute` where post_id in (" . implode(',', $posts_id) . ") and language_id = '" . (int)$this->config->get('config_language_id') . "'");
            foreach ($query->rows as $row){
                $out[$row['attribute_id']] = $row;
            }
        }
        return $out;
    }
	public function getImages($posts_id){
	    $out = [];
        if(is_array($posts_id) && count($posts_id) > 0) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_image` where post_id in (" . implode(',', $posts_id) . ")");
            foreach ($query->rows as $row) {
                $out[$row['post_image_id']] = $row;
            }
        }
        return $out;
    }
    public function getPostSeoUrls($post_id) {
        $post_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'post_id=" . (int)$post_id . "'");

        foreach ($query->rows as $result) {
            $post_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $post_seo_url_data;
    }
}