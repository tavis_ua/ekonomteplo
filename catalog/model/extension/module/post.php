<?php
class ModelExtensionModulePost extends Model {

	public function getPosts($post_type) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "post` p 
            LEFT JOIN `" . DB_PREFIX . "post_description` d on d.post_id = p.post_id";
        if($post_type == 2){
            $sql .= " INNER JOIN `" . DB_PREFIX . "post_to_service` pts on pts.post_id = p.post_id";
        }
        $sql .= " AND language_id = '" . (int)$this->config->get('config_language_id') . "'";
	    $query = $this->db->query($sql);

	    $out = [];
	    foreach ($query->rows as $row){
	        $out[$row['post_id']] = $row;
        }
		return $out;
	}

    public function getPostsByService($service_id){
        $query = $this->db->query("SELECT post_id from " . DB_PREFIX . "post_to_service where service_id = ". $service_id);
        $out = [];
        foreach ($query->rows as $item){
            $out[] = $item['post_id'];
        }
        return $out;
    }

    public function getServiceStructure($parent_id = 0){
        $sql = "SELECT * FROM " . DB_PREFIX . "service s LEFT JOIN " .
            DB_PREFIX . "service_description sd ON (s.service_id = sd.service_id)" .
            " WHERE s.parent_id = '" .
            (int)$parent_id . "' AND sd.language_id = '" . (int)$this->config->get('config_language_id') .
            "' AND s.status = '1' ORDER BY s.sort_order, LCASE(sd.name)";
        $query = $this->db->query($sql);

        foreach ($query->rows as &$item){
            $item['posts'] = implode(',', $this->getPostsByService($item['service_id']));
        }
        return $query->rows;
    }
	public function getAttributes($posts_id){
        $out = [];
        if(is_array($posts_id) && count($posts_id) > 0) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_attribute` where post_id in (" . implode(',', $posts_id) . ") and language_id = '" . (int)$this->config->get('config_language_id') . "'");
            foreach ($query->rows as $row){
                $out[$row['attribute_id']] = $row;
            }
        }
        return $out;
    }
	public function getImages($posts_id){
	    $out = [];
        if(is_array($posts_id) && count($posts_id) > 0) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_image` where post_id in (" . implode(',', $posts_id) . ")");
            foreach ($query->rows as $row) {
                $out[$row['post_image_id']] = $row;
            }
        }
        return $out;
    }
    public function getPostSeoUrls($post_id) {
        $post_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'post_id=" . (int)$post_id . "'");

        foreach ($query->rows as $result) {
            $post_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $post_seo_url_data;
    }
}