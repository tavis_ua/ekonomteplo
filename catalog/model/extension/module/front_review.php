<?php

class ModelExtensionModuleFrontReview extends Model
{

    public function getReviews($post_type, $category_id = 0)
    {
        $sql = '';
        if (in_array($post_type, [2,4]) ) {
            $sql = "select opr.*, p.image, opd.name title from oc_post p
            LEFT JOIN oc_post_review opr on p.post_id = opr.post_id
            LEFT JOIN oc_post_description opd on p.post_id = opd.post_id
            where p.post_type = {$post_type}
            and opd.language_id = " . (int)$this->config->get('config_language_id') . "
            and opr.status = 1
            ORDER BY opr.date_added desc
            LIMIT 5";
        } elseif ($post_type == 0) {
            $sql = "select op.product_id, op.image, op.price, opd.name title, oc_review.* from oc_review
                inner join oc_product op on oc_review.product_id = op.product_id 
                left join oc_product_description opd on oc_review.product_id = opd.product_id";
            if(!empty($category_id)){
                $sql .= " inner join oc_product_to_category otc on otc.product_id = op.product_id";
            }
            $sql .= " where 1 and opd.language_id = " . (int)$this->config->get('config_language_id');

            if(!empty($category_id)){
                $sql .= " and otc.category_id = " . $category_id;
            }
            $sql .= " order by oc_review.date_added desc limit 5";
        }
        if(empty($sql)) {
            return [];
        }
        $query = $this->db->query($sql);
        $user_token = '';
        if (isset($this->session->data['user_token'])) {
            $user_token = 'user_token=' . $this->session->data['user_token'];
        }
        foreach ($query->rows as $key => $row) {
            $url = '';
            if ($post_type == 1) {
                $url = $this->url->link('post/post', $user_token . 'post_id=' . $row['post_id'], true);
                $url_query = $this->db->query("select keyword from " . DB_PREFIX . "seo_url where query = 'post_id=" . $row['post_id'] . "'");
            } elseif ($post_type == 0) {
                $url = $this->url->link('product/product', $user_token . 'post_id=' . $row['product_id'], true);
                $url_query = $this->db->query("select keyword from " . DB_PREFIX . "seo_url where query = 'product_id=" . $row['product_id'] . "'");
            }
            if (!empty($url_query) && is_object($url_query) && $url_query->num_rows > 0) {
                if ($query->row) {
                    $url = '/' . $url_query->row['keyword'];
                }
            }
            $query->rows[$key]['url'] = $url;
        }
        return $query->rows;
    }

    public function getPosts($post_type)
    {

        $sql = "select p.*, pd.description from " . DB_PREFIX . "post p
        LEFT JOIN  " . DB_PREFIX . "post_description pd on pd.post_id = p.post_id
        where p.post_type = " . $post_type;
        $sql .= " AND pd.language_id = " . (int)$this->config->get('config_language_id');
        $query = $this->db->query($sql);

        $out = [];
        foreach ($query->rows as $row) {
            $query_attributes = $this->db->query("select trim(name) name, trim(text) text from " . DB_PREFIX . "post_attribute where post_id = " . $row['post_id']);
            $row['description'] = html_entity_decode($row['description']);
            $url = $this->url->link('post/post', 'user_token=' . $this->session->data['user_token'] . '&post_id=' . $row['post_id'], true);
            $url_query = $this->db->query("select keyword from " . DB_PREFIX . "seo_url where query = 'post_id=" . $row['post_id'] . "'");
            if ($url_query->num_rows > 0) {
                if ($query->row) {
                    $url = '/' . $url_query->row['keyword'];
                }
            }
            $row['url'] = $url;
            if (count($query_attributes->rows)) {
                $row['attributes'] = $query_attributes->rows;
            }
            $out[$row['post_id']] = $row;
        }
        return $out;
    }

    public function getAttributes($posts_id)
    {
        $out = [];
        if (is_array($posts_id) && count($posts_id) > 0) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_attribute` where post_id in (" . implode(',', $posts_id) . ") and language_id = '" . (int)$this->config->get('config_language_id') . "'");
            foreach ($query->rows as $row) {
                $out[$row['attribute_id']] = $row;
            }
        }
        return $out;
    }

    public function getImages($posts_id)
    {
        $out = [];
        if (is_array($posts_id) && count($posts_id) > 0) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "post_image` where post_id in (" . implode(',', $posts_id) . ")");
            foreach ($query->rows as $row) {
                $out[$row['post_image_id']] = $row;
            }
        }
        return $out;
    }

    public function getPostSeoUrls($post_id)
    {
        $post_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'post_id=" . (int)$post_id . "'");

        foreach ($query->rows as $result) {
            $post_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $post_seo_url_data;
    }
}