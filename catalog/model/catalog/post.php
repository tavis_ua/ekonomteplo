<?php
class ModelCatalogPost extends Model {

    /**
     * @param $post_id
     * @return mixed
     */
    public function getPost($post_id)
    {
        $sql = "SELECT p.*, pd.*, pp.*, floor(sum(opr.rating)/count(opr.rating)) rating  FROM " . DB_PREFIX . "post p 
        LEFT JOIN " . DB_PREFIX . "post_description pd ON (p.post_id = pd.post_id) 
        LEFT JOIN " . DB_PREFIX . "post_properties pp ON (p.post_id = pp.post_id) 
        LEFT JOIN " . DB_PREFIX . "post_review opr on p.post_id = opr.post_id 
        WHERE p.post_id = " . (int)$post_id . " AND pd.language_id = " . (int)$this->config->get('config_language_id');
        $sql .= " group by p.post_id";
        $query = $this->db->query($sql);
        $out = $query->row;
        //review
        $query = $this->db->query("SELECT * from " . DB_PREFIX . "post_review where post_id = " . $post_id);
        foreach ($query->rows as $row){
            $out['reviews'][] = $row;
        }
        //products
        $out['products'] = [];
        $query = $this->db->query("SELECT opd.product_id, language_id, name, description, tag, meta_title, meta_description, meta_keyword, meta_h1, oc_post_product.count from oc_post_product
                LEFT JOIN oc_product on oc_product.product_id = oc_post_product.product_id
                LEFT JOIN oc_product_description opd on oc_post_product.product_id = opd.product_id
                WHERE oc_post_product.post_id = " . $post_id . " AND opd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        $out['products'] = $query->rows;
        $this->load->model('catalog/product');

        foreach ($out['products'] as $key => $product){
            $productInfo = $this->model_catalog_product->getProduct($product['product_id']);
            $productAttributes = $this->model_catalog_product->getProductAttributes($product['product_id']);
            $out['products'][$key]['attributes'] = empty($productAttributes) ? [] : $productAttributes[0]['attribute'];
            $out['products'][$key]['image'] = '/image/'.$productInfo['image'];
            $out['products'][$key]['stock_status'] = $productInfo['stock_status'];
            $out['products'][$key]['price'] = $this->currency->format($this->tax->calculate($productInfo['price'], $productInfo['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            $out['products'][$key]['special'] = $this->currency->format($this->tax->calculate($productInfo['special'], $productInfo['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        }
        //seo/url
        $query = $this->db->query("select keyword from " . DB_PREFIX . "seo_url where query = 'post_id={$post_id}'");
        if ($query->row){
            $out['url'] = '/'.$query->row['keyword'];
        }else{
            $out['url'] = $this->url->link('post/post',  (empty($this->session->data['user_token']) ? '' : 'user_token=' . $this->session->data['user_token']) . '&post_id='.$post_id, true);
        }
        return $out;
    }

    public function getCategory($post_category_id) {
        $sql = "SELECT DISTINCT * FROM " .
            DB_PREFIX . "post_category c LEFT JOIN " .
            DB_PREFIX . "post_category_description cd ON (c.post_category_id = cd.post_category_id) LEFT JOIN " .
            DB_PREFIX . "seo_url su ON (concat('post_category_id=', c.post_category_id) = su.query)
            WHERE c.post_category_id = '" .
            (int)$post_category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') .
            "' AND c.status = '1'";
        $query = $this->db->query($sql);
        $out = $query->row;

        return $out;
    }
    
    public function getCategories($parent_id = 0) {
        $sql = "SELECT * FROM " . DB_PREFIX . "post_category c LEFT JOIN " .
            DB_PREFIX . "post_category_description cd ON (c.post_category_id = cd.post_category_id) LEFT JOIN " .
            DB_PREFIX . "seo_url su ON (concat('post_category_id=', c.post_category_id) = su.query) WHERE c.parent_id = '" .
            (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') .
            "' AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getSeoUrl($seo_url_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "seo_url` WHERE seo_url_id = '" . (int)$seo_url_id . "'");

        return $query->row;
    }

    public function getSeoUrlByQuery($query) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "seo_url` WHERE query = '" . $query . "' limit 1");

        return $query->row;
    }

    public function getPostDescriptions($post_id) {
        $post_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_description WHERE post_id = '" . (int)$post_id . "'");

        foreach ($query->rows as $result) {
            $post_description_data[$result['language_id']] = array(
                'name'             => $result['name'],
                'sub_name'         => $result['sub_name'],
                'description'      => $result['description'],
                'meta_title'       => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword'     => $result['meta_keyword'],
                'tag'              => $result['tag'],
                'text'             => $result['text'],
            );
        }

        return $post_description_data;
    }

    /**
     * @param $post_id
     * @return mixed
     */
    public function getImages($post_id){
        $query = $this->db->query("SELECT image from " . DB_PREFIX . "post_image where post_id=".$post_id);
        return $query->rows;
    }

    /**
     * @param $post_id
     * @return mixed
     */
    public function getArticles($post_id){
        $query = $this->db->query("SELECT distinct opd.*, oc_post.* from oc_post
            LEFT JOIN oc_post_description opd on oc_post.post_id = opd.post_id            
            WHERE oc_post.post_type = 1 ");
        return $query->rows;
    }
    /**
     * @param $post_id
     * @return mixed
     */
    public function getAttributes($post_id) {
        //attributes
        $out['attributes'] = [];
        $query = $this->db->query("SELECT * from oc_post_attribute WHERE post_id = " . $post_id . " and language_id = '" . (int)$this->config->get('config_language_id') . "'");
        return $query->rows;
    }

    public function getTotalReviewsByPostId($post_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "post_review r where post_id = '{$post_id}'");

        return $query->row['total'];
    }

    public function getReviewsByPostId($post_id, $start = 0, $limit = 20) {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 20;
        }

        $query = $this->db->query("SELECT r.post_review_id, r.author, r.rating, r.text, r.date_added 
            FROM " . DB_PREFIX . "post_review r 
            WHERE r.post_id = '" . (int)$post_id . "' AND r.status = '1'
            ORDER BY r.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getPotsCategory($filter){
        $post_list = [];
        if(!is_array($filter)){
            $post_category_id = $filter;
            $filter = [];
            $filter['post_category_id'] = $post_category_id;
        }
        $query = $this->db->query("SELECT post_id  FROM " . DB_PREFIX . "post_to_category where category_id = ".$filter['post_category_id']);
        foreach ($query->rows as $item){
            $post_list[] = $item['post_id'];
        }
        if(isset($filter['product_id']) && !empty($filter['product_id'])){
            $query = $this->db->query("SELECT post_id from " . DB_PREFIX . "post_product WHERE product_id = ". (int)($filter['product_id']) . " AND post_id in (" . implode(', ', $post_list) .')');
            $post_list = [];
            foreach ($query->rows as $item){
                $post_list[] = $item['post_id'];
            }

        }

        $out = [];
        foreach ($post_list as $pos_id){
            $out[] =  $this->getPost($pos_id);
        }
        return $out;
    }
    public function getPostCategories($post_id) {
        $post_category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "post_to_category WHERE post_id = '" . (int)$post_id . "'");

        foreach ($query->rows as $result) {
            $post_category_data[] = $result['category_id'];
        }

        return $post_category_data;
    }

    public function getPosts($data = array(), $only_top = false) {
        $sql = "SELECT * FROM " . DB_PREFIX . "post p LEFT JOIN " . DB_PREFIX . "post_description pd ON (p.post_id = pd.post_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if($only_top){
            $sql .= " AND p.is_submenu = 1";
        }

        $sql .= " GROUP BY p.post_id";

        $sort_data = array(
            'pd.name',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }
    
    public function getRelatedPosts($post_id){
        $query = $this->db->query("SELECT related_id post_id from " . DB_PREFIX . "post_related WHERE post_id=".$post_id);
        return $query->rows;
    }
}